angular.module('sellerApp.controllers', []).controller('MainContoller', function ($scope, $state, $http, $stateParams, popupService, $window) {
    $scope.config = {"config": ""};
    $http.get('js/config/frontConfig.json')
            .then(function successCallback(response) {
                $scope.config = response.data;
                $scope.title = $scope.config.title;
            }, function errorCallback(response) {
                $scope.config = response.data;
            });

}).controller('HomeController', function ($scope, $state, $cookies) {
    $scope.config.title = $scope.title + ' : Home';
    $scope.seller = $cookies.get('seller');
    if ($scope.seller) {
        $state.go('catalog');
    }

}).controller('LoginSellerController', function ($scope, $state, $stateParams) {

    $scope.formData = {};
    $scope.config.title = $scope.title + ' : Vendedor';

}).controller('authenticateSeller', function ($scope, $state, $http, $cookies) {

    $scope.submitForm = function () {
        console.log(JSON.stringify($scope.formData));

        $http.post($scope.config.accountUrl + 'authenticate/seller', JSON.stringify($scope.formData))
                .then(function successCallback(response) {
                    $cookies.putObject('sellerAuthenticaton', response.data);
                    console.log(response.data);

                }, function errorCallback(response) {

                    $scope.error = response.data;
                    console.log(response.data);

                });

    };
});
