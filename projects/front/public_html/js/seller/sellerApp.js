angular.module('sellerApp', ['ui.router', 'ngResource', 'ngCookies', 'sellerApp.controllers', 'sellerApp.services']);

angular.module('sellerApp').config(function ($stateProvider) {

    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'vendedor/partials/home.html',
        controller: 'HomeController'

    }).state('loginSeller', {
        url: '/login',
        templateUrl: 'vendedor/partials/login.html',
        controller: 'LoginSellerController'

    });

}).run(function ($state) {
    $state.go('loginSeller');
});
