function limpa_formulário_cep($scope) {
    $scope.address.street = '';
    $scope.address.neighborhood = '';
    $scope.address.city = '';
    $scope.address.state = '';
}

function setCustomerToCart($http, $scope, $rootScope, cart, authenticationToken) {
    var formData = {};
    formData.authenticationToken = authenticationToken;
    $http.put($scope.config.checkoutUrl + 'cart/' + cart.encryptId + '/customer/', JSON.stringify(formData))
            .then(function successCallback(response) {
                console.log('Carrinho atualizado: ' + JSON.stringify(response.data));
            }, function errorCallback(response) {
                console.log('Erro ao atualizar o carrinho: ' + JSON.stringify(response));
            });
}

function showMsg($rootScope, title, body) {
    $rootScope.msg = {};
    $rootScope.msg.title = title;
    $rootScope.msg.body = body;
    $('#msgModal').modal('show');
}

function addProductToCart(product, $http, $cookies, $scope, $rootScope) {
    var formData = {};
    formData.id = product.id;
    formData.quantity = product.productQuantity;
    $http.post($scope.config.checkoutUrl + 'cart/' + $cookies.getObject('cart').encryptId, JSON.stringify(formData))
            .then(function successCallback(response) {
                showMsg($rootScope, 'Produto adicionado!', product.name + ' foi adicionado com sucesso ao seu carrinho de compras');
            }, function errorCallback(response) {
                if (response.status === 404) {
                    showMsg($rootScope, 'Carrinho expirado!', 'Seu carrinho expirou devido ao tempo de inatividade no site.');
                } else if (response.status === 409) {
                    showMsg($rootScope, 'Produto já adicionado!', 'Este produto já foi adicionado ao carrinho, parar alterar a quantidade vá ao seu Carrinho de compras');
                } else {
                    showMsg($rootScope, 'Item não adicionado!', 'Não foi possível adicionar o produto ao carrinho, por favor tente novamente.');
                    console.log(JSON.stringify(response.data));
                }

            });
}

function changeProductQuantity(product, $http, $cookies, $scope, $rootScope) {
    var formData = {};
    formData.id = product.id;
    formData.quantity = product.productQuantity;
    $http.put($scope.config.checkoutUrl + 'cart/' + $cookies.getObject('cart').encryptId, JSON.stringify(formData))
            .then(function successCallback(response) {
                showMsg($rootScope, 'Produto atualizado!', 'O produto ' + product.name + ' foi atualizado com sucesso.');
                window.location.reload();
            }, function errorCallback(response) {
                if (response.status === 404) {
                    showMsg($rootScope, 'Carrinho expirado!', 'Seu carrinho expirou devido ao tempo de inatividade no site.');
                } else if (response.status === 409) {
                    showMsg($rootScope, 'Produto já adicionado!', 'Este produto já foi adicionado ao carrinho, parar alterar a quantidade vá ao seu Carrinho de compras');
                } else {
                    showMsg($rootScope, 'Item não adicionado!', 'Não foi possível adicionar o produto ao carrinho, por favor tente novamente.');
                    console.log(JSON.stringify(response.data));
                }

            });
}

function preLoad($scope, $state, $http, $cookies) {
    if ($scope.config === undefined) {
        $scope.config = {};
        $scope.config.title = '';
    }
    $http.get('js/config/frontConfig.json')
            .then(function successCallback(response) {
                $scope.config = response.data;
                $scope.title = '';
//                $scope.title = $scope.config.title;

                //Validando carrinho
                var cart = $cookies.getObject('cart');
                if (cart && cart.encryptId && $scope.config.checkoutUrl) {
                    $http.get($scope.config.checkoutUrl + 'cart/' + cart.encryptId)
                            .then(function successCallback(response) {
                                $cookies.putObject('cart', response.data);
                            }, function errorCallback(response) {
                                if (response.status === 410) {
                                    $cookies.remove("cart");
                                } else if (response.status === 404) {
                                    $cookies.remove("cart");
                                }
                            });
                }

                // validando login usuário
                var customerToken = $cookies.get('customerToken');
                if (customerToken && $scope.config.accountUrl) {

                    $http.get($scope.config.accountUrl + 'customer/' + customerToken)
                            .then(function successCallback(response) {
                                $cookies.put('customerToken', response.data.authenticationToken);
                                $scope.customer = response.data;
                            }, function errorCallback(response) {
                                console.log(JSON.stringify(response.data));
                                if (response.status === 410) {
                                    $cookies.remove("customerToken");
                                    console.log("Autenticação expirada e removida");
                                } else {
                                    $cookies.remove("customerToken");
                                    console.log("Autenticação inválida e removida");
                                }
                                window.location.reload();
                            });
                }

            }
            , function errorCallback(response) {
                $scope.config = response.data;
//                console.log('error:' + JSON.stringify($scope.config));
            });

    $scope.seller = $cookies.get('seller');
    if (!$scope.seller) {
        $state.go('home');
        return;
    }

}

angular.module('frontApp.controllers', []).controller('MainContoller', function ($scope, $state, $http, $cookies, $stateParams, popupService, $window) {
    preLoad($scope, $state, $http, $cookies);

}).controller('HomeController', function ($scope, $state, $cookies, $http) {
    preLoad($scope, $state, $http, $cookies);
    $scope.config.title = $scope.title + ' : Home';
    seller = $cookies.get('seller');
    if (seller) {
        $state.go('catalog');
    }

}).controller('findSeller', function ($scope, $state, $http, $cookies) {

    $scope.submitForm = function () {
        $http.get($scope.config.accountUrl + 'seller/' + $scope.form.id)
                .then(function successCallback(response) {
                    $cookies.putObject('seller', response.data);
                    $state.go('catalog'); //State
                }, function errorCallback(response) {
                    if (response.status === 404) {
                        $scope.error = response.data;
                    }
                });

    };

}).controller('findSellerFromUrl', function ($scope, $state, $http, $cookies, $stateParams) {
    var codeOrNick = ''
    if ($stateParams !== undefined && $stateParams.codOrNick !== undefined) {
        codeOrNick = $stateParams.codOrNick;
        if (codeOrNick === '') {
            $state.go('home');
        }
    } else {
        $state.go('home');
    }

    $http.get($scope.config.accountUrl + 'seller/' + codeOrNick)
            .then(function successCallback(response) {
                $cookies.putObject('seller', response.data);
                console.log('sucesso: ' + JSON.stringify(response.data));
                $state.go('catalog'); //State
            }, function errorCallback(response) {
                console.log('error: ' + JSON.stringify(response.data));
                if (response.status === 404) {
                    $scope.error = response.data;
                }
            });

}).controller('CatalogController', function ($scope, $cookies, $state, $http) {
    //Catálogo
    preLoad($scope, $state, $http, $cookies);

    $scope.config.title = $scope.title + ' : Catálogo';
    $scope.seller = $cookies.getObject('seller');

    $http.get($scope.config.stockUrl + 'category')
            .then(function (response) {
                $scope.categories = response.data;
            });

}).controller('CategoryViewController', function ($scope, $state, $http, $cookies, $stateParams) {
    preLoad($scope, $state, $http, $cookies);
    $http.get($scope.config.stockUrl + 'category/' + $stateParams.id)
            .then(function (response) {
                $scope.category = response.data;
                $scope.config.title = $scope.title + ' : ' + $scope.category.name;
            });

    $http.get($scope.config.stockUrl + 'product/category/' + $stateParams.id)
            .then(function successCallback(response) {
                $scope.products = response.data;
                console.log('Produtos: ' + JSON.stringify($scope.products));

            }, function errorCallback(response) {
                $scope.error = response.data;
                console.log('Erro ao carregar os produtos: ' + JSON.stringify($scope.error));
            });

}).controller('addToCartController', function ($scope, $state, $http, $cookies, $rootScope) {

    $scope.submitForm = function (product) {

        //Só cria um carrinho se ja não tem um criado no cookie
        var formData = {};
        formData.customerId = 0;
        formData.sellerId = 0;

        if ($scope.customer) {
            formData.customerId = $scope.customer.id;
        }

        var seller = $cookies.getObject('seller');
        if (seller) {
            formData.sellerId = seller.id;
        }

        if ($cookies.getObject('cart') === undefined) {

            //Cria o carrinho para o usuário
            $http.post($scope.config.checkoutUrl + 'cart', JSON.stringify(formData))
                    .then(function successCallback(response) {
                        $cookies.putObject('cart', response.data);
                        addProductToCart(product, $http, $cookies, $scope, $rootScope);
                    }, function errorCallback(response) {
                        showMsg($rootScope, 'Falha ao criar o carrinho. Se o erro persistir, contate o administrador.');
                        console.log(JSON.stringify(response.data));
                    });
        } else {
            addProductToCart(product, $http, $cookies, $scope, $rootScope);
        }

    };

}).controller('updateCartController', function ($scope, $state, $http, $cookies, $rootScope) {

    $scope.submitForm = function (product) {

        //Só cria um carrinho se ja não tem um criado no cookie
        formData = {};
        formData.customerId = 0;

        if ($scope.customer) {
            formData.customerId = $scope.customer.id;
        }

        if ($cookies.getObject('cart') === undefined) {

            //Cria o carrinho para o usuário
            $http.post($scope.config.checkoutUrl + 'cart', JSON.stringify(formData))
                    .then(function successCallback(response) {
                        $cookies.putObject('cart', response.data);
                        changeProductQuantity(product, $http, $cookies, $scope, $rootScope);
                    }, function errorCallback(response) {
                        $scope.error = response.data;
                        console.log(response.data);
                    });
        } else {
            changeProductQuantity(product, $http, $cookies, $scope, $rootScope);
        }

    };

}).controller('LoginCustomerController', function ($scope) {

    $scope.formData = {};

}).controller('authenticateCustomer', function ($scope, $http, $cookies, $rootScope) {
    $scope.submitForm = function () {
        console.log(JSON.stringify($scope.formData));

        $http.post($scope.config.accountUrl + 'authenticate/customer', JSON.stringify($scope.formData))
                .then(function successCallback(response) {
                    $cookies.put('customerToken', response.data.authenticationToken);

                    var cart = $cookies.getObject('cart');

                    if (cart !== undefined) {
                        var formData = {};
                        formData.authenticationToken = response.data.authenticationToken;
                        $http.put($scope.config.checkoutUrl + 'cart/' + cart.encryptId + '/customer/', JSON.stringify(formData))
                                .then(function successCallback(response) {
                                    console.log('Carrinho atualizado: ' + JSON.stringify(response.data));
                                }, function errorCallback(response) {
                                    console.log('Erro ao atualizar o carrinho: ' + JSON.stringify(response));
                                });
//                        setCustomerToCart($http, $scope, $rootScope, cart, response.data.authenticationToken);
                    }

                    window.location.reload();

                }, function errorCallback(response) {

                    $scope.error = response.data;
                    console.log(JSON.stringify(response.data));

                });

    };

}).controller('logoutCustomerController', function ($state, $scope, $http, $cookies, $rootScope) {
    $cookies.remove('customerToken');
    window.location.reload();

}).controller('CartViewController', function ($scope, $state, $http, $cookies, $stateParams, $rootScope) {
    preLoad($scope, $state, $http, $cookies);
    $scope.address = {};

    $scope.config.title = $scope.title + ' : Carrinho';

    var cart = $cookies.getObject('cart');

    if (cart === undefined) {
        cart = {};
    } else {
        $http.get($scope.config.checkoutUrl + 'cart/' + cart.encryptId + '/fullCart')
                .then(function successCallback(response) {
                    $scope.cart = response.data;

                    var customerToken = $cookies.get('customerToken');
                    if (customerToken && $scope.config.accountUrl) {

                        $http.get($scope.config.accountUrl + 'address/customer/' + customerToken)
                                .then(function successCallback(response) {
                                    $scope.addresses = response.data;
                                    $scope.address = response.data[0];
                                    if ($scope.address !== undefined) {
                                        $http.get($scope.config.checkoutUrl + 'freight/' + $scope.address.zipCode + '/cart/' + cart.encryptId)
                                                .then(function successCallback(response) {
                                                    $scope.cart.freightCost = response.data.pac;

                                                }, function errorCallback(response) {
                                                    $scope.error = response.data;

                                                });
                                    }


                                }, function errorCallback(response) {
                                    console.log(JSON.stringify(response.data));
                                });
                    }

                }, function errorCallback(response) {

                    $scope.error = response.data;
                    console.log(JSON.stringify(response.data));

                });
    }

    $scope.completeAddress = function () {

        //Nova variável "cep" somente com dígitos.
        var cep = $scope.address.zipCode.replace(/\D/g, '');
        //Verifica se campo cep possui valor informado.
        if (cep !== "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;
            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $scope.address.street = 'buscando...';
                $scope.address.neighborhood = 'buscando...';
                $scope.address.city = 'buscando...';
                $scope.address.uf = '';

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $scope.address.street = dados.logradouro;
                        $scope.address.neighborhood = dados.bairro;
                        $scope.address.city = dados.localidade;
                        $scope.address.uf = dados.uf.toUpperCase();
                    }
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep($scope);
                        alert("CEP não encontrado.");
                    }
                });

                $http.get($scope.config.checkoutUrl + 'freight/' + cep + '/cart/' + cart.encryptId)
                        .then(function successCallback(response) {
                            $scope.cart.freightCost = response.data.pac;
                        }, function errorCallback(response) {
                            $scope.error = response.data;
                        });

            } else if (cep.length < 8) {
                return;
            } else {
                //cep é inválido.
                limpa_formulário_cep($scope);
                alert("Formato de CEP inválido.");
            }
        }
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep($scope);
            $scope.address.city = 'RJ';
        }
    };

    $scope.updateCart = function (product) {
        changeProductQuantity(product, $http, $cookies, $scope, $rootScope);
        var cep = $scope.address.zipCode.replace(/\D/g, '');
        $http.get($scope.config.checkoutUrl + 'freight/' + cep + '/cart/' + cart.encryptId)
                .then(function successCallback(response) {
                    $scope.cart.freightCost = response.data.pac;
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    };

    $scope.removeToCart = function (product) {
        product.productQuantity = 0;
        changeProductQuantity(product, $http, $cookies, $scope, $rootScope);
        var cep = $scope.address.zipCode.replace(/\D/g, '');
        $http.get($scope.config.checkoutUrl + 'freight/' + cep + '/cart/' + cart.encryptId)
                .then(function successCallback(response) {
                    $scope.cart.freightCost = response.data.pac;
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    };

}).controller('paymentController', function ($scope, $http, $cookies, $state) {
    $scope.tiketPayment = function (address) {
        console.log(JSON.stringify(address));
        var cart = $cookies.getObject('cart');
        var customerToken = $cookies.get('customerToken');
        var formData = {};
        formData.address = address;
        formData.authenticationToken = customerToken;
        $http.post($scope.config.checkoutUrl + 'cart/' + cart.encryptId + '/tiketPayment', JSON.stringify(formData))
                .then(function successCallback(response) {
                    $state.go('order', {id: response.data.orderToken});
                    window.open($scope.config.checkoutUrl + 'order/' + customerToken + '/' + response.data.orderToken + '/viewTiket', '_blank');
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    };

}).controller('myAccountController', function ($scope, $http, $cookies) {

}).controller('myOrdersController', function ($scope, $http, $cookies) {
    var customerToken = $cookies.get('customerToken');

    if (customerToken !== undefined) {
        $http.get($scope.config.checkoutUrl + 'order/' + customerToken)
                .then(function successCallback(response) {
                    var orders = response.data;
                    JSON.stringify(orders);
                    for (i = 0; i < orders.length; i++) {
                        orders[i].status = 'Aguardando Pagamento';
                        orders[i].class = 'warning';
                        if (orders[i].paid) {
                            orders[i].status = 'Em preparação';
                            orders[i].class = 'active';
                        }
                        if (orders[i].packaged) {
                            orders[i].status = 'Aguardando envio';
                            orders[i].class = 'active';
                        }
                        if (orders[i].sent) {
                            orders[i].status = 'Enviado';
                            orders[i].class = 'active';
                        }
                        if (orders[i].delivered) {
                            orders[i].status = 'Entregue';
                            orders[i].class = 'success';
                        }
                        $scope.orders = orders;
                    }
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    }

}).controller('orderController', function ($scope, $http, $cookies, $stateParams) {
    var customerToken = $cookies.get('customerToken');

    if ($stateParams !== undefined && $stateParams.id !== undefined && customerToken !== undefined) {
        var orderId = $stateParams.id;

        $http.get($scope.config.checkoutUrl + 'order/' + customerToken + '/' + orderId)
                .then(function successCallback(response) {
                    $scope.order = response.data;
                    $scope.address = JSON.parse($scope.order.deliveryAddress);
                    $scope.boleto = $scope.config.checkoutUrl + 'order/' + customerToken + '/' + orderId + '/viewTiket';
                }, function errorCallback(response) {
                    $scope.error = response.data;
                });
    }

});