angular.module('managerApp', ['ui.router', 'ngResource', 'ngCookies', 'managerApp.controllers', 'managerApp.services']);

angular.module('managerApp').config(function ($stateProvider) {

    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'administrador/partials/home.html',
        controller: 'HomeController'

    }).state('loginManager', {
        url: '/login',
        templateUrl: 'administrador/partials/login.html',
        controller: 'LoginManagerController'

    }).state('category', {
        url: '/cadastro/categoria',
        templateUrl: 'administrador/partials/category.html',
        controller: 'categoryController'

    }).state('logoutManager', {
        url: '/logout',
        templateUrl: 'administrador/partials/login.html',
        controller: 'logoutManagerController'

    });

}).run(function ($state) {
    $state.go('loginManager');
});
