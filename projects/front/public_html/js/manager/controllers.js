function preLoad($scope, $state, $http, $cookies) {
    if ($scope.config === undefined) {
        $scope.config = {};
        $scope.config.title = '';
    }
    $http.get('js/config/frontConfig.json')
            .then(function successCallback(response) {
                $scope.config = response.data;
                $scope.title = '';

                // validando login usuário
                var customerToken = $cookies.get('managerToken');
                if (customerToken && $scope.config.accountUrl) {

                    $http.get($scope.config.accountUrl + 'manager/' + customerToken)
                            .then(function successCallback(response) {
                                $cookies.put('managerToken', response.data.authenticationToken);
                                $scope.customer = response.data;

                            }, function errorCallback(response) {
                                console.log(JSON.stringify(response.data));
                                if (response.status === 410) {
                                    $cookies.remove("managerToken");
                                    console.log("Autenticação expirada e removida");
                                } else {
                                    $cookies.remove("managerToken");
                                    console.log("Autenticação inválida e removida");
                                }
                                window.location.reload();
                            });
                } else {
                    $state.go('loginManager');
                }

            }
            , function errorCallback(response) {
                $scope.config = response.data;
            });

}

angular.module('managerApp.controllers', []).controller('MainContoller', function ($scope, $state, $http, $stateParams, popupService, $window, $cookies) {
    preLoad($scope, $state, $http, $cookies);

}).controller('HomeController', function ($scope, $state, $cookies, $http) {
    preLoad($scope, $state, $http, $cookies);
    $scope.config.title = $scope.title + ' : Home';

}).controller('LoginManagerController', function ($scope, $state, $stateParams) {

    $scope.formData = {};

    $scope.config.title = $scope.title + ' : Administrador';

}).controller('authenticateManager', function ($scope, $state, $http, $cookies) {

    $scope.submitForm = function () {
        console.log(JSON.stringify($scope.formData));

        $http.post($scope.config.accountUrl + 'authenticate/manager', JSON.stringify($scope.formData))
                .then(function successCallback(response) {
                    $cookies.putObject('managerToken', response.data.authenticationToken);
                    $state.go('home');
                    window.alert('autenticado');

                    return;

                }, function errorCallback(response) {
                    $scope.error = response.data;
                    console.log(JSON.stringify(response.data));

                });

    };

}).controller('logoutManagerController', function ($state, $scope, $http, $cookies, $rootScope) {
    $cookies.remove('customerToken');
    $state.go('home');
    window.location.reload();

}).controller('categoryController', function ($scope, $state, $stateParams) {

    $scope.config.title = $scope.title + ' : Cadastro : Categoria';
});
