package br.com.account.domain;

import br.com.commons.json.SellerJson;
import br.com.commons.security.Encrypter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "SELLER")
@PrimaryKeyJoinColumn(name = "CD_ACCOUNT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Seller extends Account implements Authenticatable, Serializable {

    private static final long serialVersionUID = -8819156267205331380L;

    @Column(name = "NM_CODE", nullable = false, unique = true)
    private String code;

    public Seller() {
    }

    public Seller(SellerJson sellerJson) throws Exception {
        this.setId(sellerJson.getId());
        this.setCpf(sellerJson.getCpf());
        this.setEmail(sellerJson.getEmail());
        this.setNickName(sellerJson.getNickName());
        this.setPassword(Encrypter.encrypt(sellerJson.getPassword()));
        this.setName(sellerJson.getName());
        this.setCode(sellerJson.getCode());
    }

    @Override
    public SellerJson toJson() {
        SellerJson sellerJson = new SellerJson(super.toJson());
        sellerJson.setCode(getCode());

        return sellerJson;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
