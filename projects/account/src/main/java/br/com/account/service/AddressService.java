package br.com.account.service;

import br.com.account.dao.interf.IAddressDao;
import br.com.account.domain.Address;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

    @Autowired
    private IAddressDao addressDao;   
    
    public Address create(Address address) throws Exception {
        address.setId(null);
        return addressDao.save(address);
    }
    
    public List<Address> findByCustomerId(Long customerId) throws Exception{
        return addressDao.findByCustomerId(customerId);
    }
}
