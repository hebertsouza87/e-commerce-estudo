package br.com.account.dao.interf;

import br.com.account.domain.Authenticatable;
import java.util.List;

public interface Authenticate {

    public List<Authenticatable> findByEmailAndPassword(String email, String password);

}
