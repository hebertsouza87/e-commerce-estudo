package br.com.account.controller;

import br.com.account.domain.Address;
import br.com.account.domain.Customer;
import br.com.account.domain.Manager;
import br.com.account.domain.Seller;
import br.com.account.service.AddressService;
import br.com.account.service.CustomerService;
import br.com.account.service.ManagerService;
import br.com.account.service.SellerService;
import br.com.commons.json.AccountJson;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class InitController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SellerService sellerService;

    @Autowired
    private ManagerService managerService;

    @Autowired
    private AddressService addressService;

    @RequestMapping(value = "/init", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<AccountJson>> init(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<AccountJson> list = new ArrayList<>(3);

        Customer customer = new Customer();
        customer.setCpf("08151957611");
        customer.setEmail("cliente@teste.com");
        customer.setName("Cliente Teste");
        customer.setNickName("C Teste");
        customer.setPassword("12341234");

        Address address = new Address();
        address.setActive(Boolean.TRUE);
        address.setZipCode("89012510");
        address.setUF("SC");
        address.setCity("Blumenau");
        address.setNeighborhood("Victor Konder");
        address.setStreet("Rua Max Hering");
        address.setNumber("19");
        address.setComplement("Apto 401");
        address.setRecipient("Cliente");

        try {
            customer = customerService.create(customer);
            address.setCustomer(customer);
            addressService.create(address);
            customer.setAddresses(Arrays.asList(address));
            customer = customerService.update(customer);
        } catch (Exception e) {
            System.out.println(e);
        }
        list.add(customer.toJson());

        Seller seller = new Seller();
        seller.setCpf("69353989086");
        seller.setEmail("vendedor@teste.com");
        seller.setName("Vendedor Teste");
        seller.setNickName("V Teste");
        seller.setPassword("12341234");
        seller.setCode("0");
        try {
            seller = sellerService.create(seller);
        } catch (Exception e) {
            System.out.println(e);
        }
        list.add(seller.toJson());

        Manager manager = new Manager();
        manager.setCpf("57006358280");
        manager.setEmail("administrador@teste.com");
        manager.setName("Administrador Teste");
        manager.setNickName("A Teste");
        manager.setPassword("12341234");

        try {
            manager = managerService.create(manager);
        } catch (Exception e) {
            System.out.println(e);
        }
        list.add(manager.toJson());

        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
