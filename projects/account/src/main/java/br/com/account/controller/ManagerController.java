package br.com.account.controller;

import br.com.account.domain.Manager;
import br.com.account.service.AuthenticateService;
import br.com.account.service.ManagerService;
import br.com.commons.json.AuthenticationTokenJson;
import br.com.commons.json.ManagerJson;
import br.com.commons.security.Authentication;
import br.com.commons.security.AuthenticationRole;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/manager")
public class ManagerController {

    @Autowired
    private ManagerService managerService;

    @Autowired
    private AuthenticateService authenticateService;

    @RequestMapping(value = "/{token}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<ManagerJson> findByToken(@PathVariable("token") String token, HttpServletRequest request, HttpServletResponse response) throws Exception {
        token = authenticateService.validateToken(token);
        Manager manager = managerService.findByToken(token);
        ManagerJson managerJson = manager.toJson();

        managerJson.setAuthenticationToken(token);
        managerJson.setPassword(null);

        return new ResponseEntity<>(managerJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<AuthenticationTokenJson> create(@RequestBody ManagerJson managerJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Manager manager = new Manager(managerJson);
        manager = managerService.create(manager);
        Authentication authentication = new Authentication(manager.getId(), AuthenticationRole.MANAGER);

        return new ResponseEntity<>(new AuthenticationTokenJson(authentication.getAuthenticationCode()), HttpStatus.CREATED);
    }
}
