package br.com.account.domain;

import br.com.commons.json.ManagerJson;
import br.com.commons.security.Encrypter;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "MANAGER")
@PrimaryKeyJoinColumn(name = "CD_ACCOUNT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Manager extends Account implements Serializable {

    private static final long serialVersionUID = 8849537779471482397L;

    public Manager() {
    }

    public Manager(ManagerJson managerJson) throws Exception {
        this.setId(managerJson.getId());
        this.setCpf(managerJson.getCpf());
        this.setEmail(managerJson.getEmail());
        this.setNickName(managerJson.getNickName());
        this.setPassword(Encrypter.encrypt(managerJson.getPassword()));
        this.setName(managerJson.getName());
    }

    @Override
    public ManagerJson toJson() {
        return new ManagerJson(super.toJson());
    }

}
