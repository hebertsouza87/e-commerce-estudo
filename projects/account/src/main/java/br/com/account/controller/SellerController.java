package br.com.account.controller;

import br.com.account.domain.Seller;
import br.com.account.service.SellerService;
import br.com.commons.json.AuthenticationTokenJson;
import br.com.commons.json.SellerJson;
import br.com.commons.security.Authentication;
import br.com.commons.security.AuthenticationRole;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/seller")
public class SellerController {

    @Autowired
    private SellerService sellerService;

    @RequestMapping(value = "/{codeOrNick}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<SellerJson> findByCodeOrNick(@PathVariable("codeOrNick") String codeOrNick, HttpServletRequest request, HttpServletResponse response) throws Exception {
        final Seller seller = sellerService.findByNickOrCode(codeOrNick);

        //Regra de tela. Como o recurso não é protegido por senha nem todos os dados devem ser expostos
        SellerJson sellerJson = seller.toJson();
        sellerJson.setCpf(null);
        sellerJson.setPassword(null);

        return new ResponseEntity<>(sellerJson, HttpStatus.OK);
    }

    @RequestMapping(value = "/teste", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<SellerJson> teste(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Seller seller = new Seller();
        seller.setCode("123");
        seller.setCpf("00011122233");
        seller.setEmail("seller@teste.com");
        seller.setName("Seller");
        seller.setNickName("Seller Test");
        seller.setPassword("12341234");

        seller = sellerService.create(seller);

        SellerJson sellerJson = seller.toJson();

        return new ResponseEntity<>(sellerJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<AuthenticationTokenJson> create(@RequestBody SellerJson sellerJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Seller seller = new Seller(sellerJson);
        seller = sellerService.create(seller);
        Authentication authentication = new Authentication(seller.getId(), AuthenticationRole.SELLER);

        return new ResponseEntity<>(new AuthenticationTokenJson(authentication.getAuthenticationCode()), HttpStatus.CREATED);
    }

}
