package br.com.account.service;

import br.com.account.dao.interf.ICustomerDao;
import br.com.account.domain.Customer;
import br.com.commons.security.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    @Autowired
    private ICustomerDao customerDao;

    public Customer findByToken(String token) throws Exception {
        Authentication recoveredAuthentication = Authentication.fromString(token);
        return customerDao.findById(Long.valueOf(recoveredAuthentication.getId()));
    }

    public Customer create(Customer customer) throws Exception {
        customer.setId(null);
        return customerDao.save(customer);
    }

    public Customer update(Customer customer) {
        return customerDao.update(customer);
    }
}
