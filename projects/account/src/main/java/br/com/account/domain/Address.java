package br.com.account.domain;

import br.com.commons.json.AddressJson;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "ADDRESS")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Address implements Serializable {

    private static final long serialVersionUID = 4340163893350478119L;

    @Id
    @SequenceGenerator(name = "address_id", sequenceName = "address_id_seg")
    @GeneratedValue(generator = "address_id", strategy = GenerationType.AUTO)
    @Column(name = "CD_ADRRESS")
    private Long id;

    @Column(name = "NM_CITY", nullable = false)
    private String city;

    @Column(name = "NM_UF", nullable = false, length = 2)
    private String uf;

    @Column(name = "NM_NEIGHBORHOOD", nullable = false)
    private String neighborhood;

    @Column(name = "NM_STREET", nullable = false)
    private String street;

    @Column(name = "NM_NUMBER", nullable = false)
    private String number;

    @Column(name = "NM_COMPLEMENT")
    private String complement;

    @Column(name = "NM_ZIP_CODE", nullable = false)
    private String zipCode;

    @Column(name = "BO_ACTIVE", nullable = false)
    private Boolean active;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CD_CUSTOMER", nullable = false)
    private Customer customer;

    @Column(name = "NM_DESTINATARIO", nullable = false, length = 20)
    private String recipient;

    public Address() {
    }

    public Address(AddressJson addressJson) {
        setId(addressJson.getId());
        setCity(addressJson.getCity());
        setUF(addressJson.getUf());
        setNeighborhood(addressJson.getNeighborhood());
        setStreet(addressJson.getStreet());
        setNumber(addressJson.getNumber());
        setComplement(addressJson.getComplement());
        setZipCode(addressJson.getZipCode());
        setActive(addressJson.getActive());
        //customer
        setRecipient(addressJson.getRecipient());
    }

    public AddressJson toJson() {
        AddressJson addressJson = new AddressJson();
        addressJson.setId(getId());
        addressJson.setCity(getCity());
        addressJson.setUf(getUF());
        addressJson.setNeighborhood(getNeighborhood());
        addressJson.setStreet(getStreet());
        addressJson.setNumber(getNumber());
        addressJson.setComplement(getComplement());
        addressJson.setZipCode(getZipCode());
        //addressJson.setActive(getActive());
        addressJson.setCustomerId(getCustomer().getId());
        addressJson.setRecipient(getRecipient());

        return addressJson;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getUF() {
        return uf;
    }

    public void setUF(String uf) {
        this.uf = uf;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }
}
