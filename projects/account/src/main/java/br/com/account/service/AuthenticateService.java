package br.com.account.service;

import br.com.account.dao.CustomerDao;
import br.com.account.dao.ManagerDao;
import br.com.account.dao.SellerDao;
import br.com.account.domain.Authenticatable;
import br.com.commons.exception.ConflictException;
import br.com.commons.exception.ExpiredException;
import br.com.commons.exception.ForbidenException;
import br.com.commons.json.AuthenticateJson;
import br.com.commons.security.Authentication;
import br.com.commons.security.AuthenticationRole;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class AuthenticateService {

    @Autowired
    private SellerDao sellerDao;

    @Autowired
    private ManagerDao managerDao;

    @Autowired
    private CustomerDao customerDao;

    public String authenticate(AuthenticateJson authenticateJson, AuthenticationRole authenticationRole) throws Exception {

        List<Authenticatable> authenticatables = new ArrayList<>(0);

        if (AuthenticationRole.SELLER.equals(authenticationRole)) {
            authenticatables = sellerDao.findByEmailAndPassword(authenticateJson.getEmail(), authenticateJson.getPassword());

        } else if (AuthenticationRole.MANAGER.equals(authenticationRole)) {
            authenticatables = managerDao.findByEmailAndPassword(authenticateJson.getEmail(), authenticateJson.getPassword());

        } else if (AuthenticationRole.CUSTOMER.equals(authenticationRole)) {
            authenticatables = customerDao.findByEmailAndPassword(authenticateJson.getEmail(), authenticateJson.getPassword());
        }

        if (authenticatables.isEmpty()) {
            throw new ForbidenException("Usuário/senha inválidos.");
        }

        if (authenticatables.size() > 1) { // Nunca deverá ocorrer devido a não ser permitido repetir o e-mail
            throw new ConflictException("Houve um erro com sua authenticação, favor entre em contato com o administrador");
        }

        Authentication authentication = new Authentication(authenticatables.get(0).getId(), authenticationRole);

        return authentication.getAuthenticationCode();
    }

    public String validateToken(String token) throws Exception {
        Authentication recoveredAuthentication = Authentication.fromString(token);
        if (recoveredAuthentication.isExpired()) {
            throw new ExpiredException("A autenticação deste usuário está expirada");
        }
        recoveredAuthentication.updateExpirationDate();
        return recoveredAuthentication.getAuthenticationCode();
    }

}
