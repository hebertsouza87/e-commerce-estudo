package br.com.account.controller;

import br.com.account.domain.Customer;
import br.com.account.service.AuthenticateService;
import br.com.account.service.CustomerService;
import br.com.commons.json.AuthenticationTokenJson;
import br.com.commons.json.CustomerJson;
import br.com.commons.security.Authentication;
import br.com.commons.security.AuthenticationRole;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AuthenticateService authenticateService;

    @RequestMapping(value = "/{token}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<CustomerJson> findByToken(@PathVariable("token") String token, HttpServletRequest request, HttpServletResponse response) throws Exception {
        token = authenticateService.validateToken(token);
        Customer customer = customerService.findByToken(token);
        CustomerJson customerJson = customer.toJson();

        customerJson.setAuthenticationToken(token);
        customerJson.setPassword(null);

        return new ResponseEntity<>(customerJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<AuthenticationTokenJson> create(@RequestBody CustomerJson customerJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Customer customer = new Customer(customerJson);
        customer = customerService.create(customer);
        Authentication authentication = new Authentication(customer.getId(), AuthenticationRole.CUSTOMER);

        return new ResponseEntity<>(new AuthenticationTokenJson(authentication.getAuthenticationCode()), HttpStatus.CREATED);
    }

}
