package br.com.account.service;

import br.com.account.dao.interf.IManagerDao;
import br.com.account.domain.Manager;
import br.com.commons.security.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManagerService {

    @Autowired
    private IManagerDao managerDao;

    public Manager create(Manager manager) {
        return managerDao.save(manager);
    }

    public Manager findByToken(String token) throws Exception {
        Authentication recoveredAuthentication = Authentication.fromString(token);
        return managerDao.findById(Long.valueOf(recoveredAuthentication.getId()));
    }
}
