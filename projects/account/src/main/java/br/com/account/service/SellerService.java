package br.com.account.service;

import br.com.account.dao.interf.ISellerDao;
import br.com.account.domain.Seller;
import br.com.commons.exception.ConflictException;
import br.com.commons.exception.NotFoundException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SellerService {

    @Autowired
    private ISellerDao sellerDao;

    public Seller findByNickOrCode(String nickOrCode) throws NotFoundException, ConflictException {
        List<Seller> sellers = sellerDao.findByCodeOrNick(nickOrCode);

        if (sellers == null || sellers.isEmpty()) {
            throw new NotFoundException("Vendedor não encontrado, favor tente outro código/nick");
        } else if (sellers.size() > 1) {
            throw new ConflictException();
        } else {
            return sellers.get(0);
        }
    }

    public Seller create(Seller seller) {
        return sellerDao.save(seller);
    }
}
