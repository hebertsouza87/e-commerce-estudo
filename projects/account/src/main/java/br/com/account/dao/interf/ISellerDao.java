package br.com.account.dao.interf;

import br.com.account.domain.Authenticatable;
import br.com.account.domain.Seller;
import java.util.List;

public interface ISellerDao {

    public Seller save(Seller seller);

    public List<Seller> findByCodeOrNick(String codeOrNick);

    public List<Authenticatable> findByEmailAndPassword(String email, String password);

    public List<Seller> findByCpf(String cpf);

}
