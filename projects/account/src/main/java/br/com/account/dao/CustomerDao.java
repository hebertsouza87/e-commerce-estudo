package br.com.account.dao;

import br.com.account.dao.interf.Authenticate;
import br.com.account.dao.interf.ICustomerDao;
import br.com.account.domain.Authenticatable;
import br.com.account.domain.Customer;
import br.com.commons.dao.AbstractDao;
import br.com.commons.security.Encrypter;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class CustomerDao extends AbstractDao<Customer> implements ICustomerDao, Authenticate {

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Customer save(Customer customer) {
        return super.save(customer);
    }

    @Override
    public List<Authenticatable> findByEmailAndPassword(String email, String password) {

        List<Criterion> restrictions = new ArrayList<>();

        LogicalExpression logicAnd = Restrictions.and(Restrictions.eq("email", email).ignoreCase(), Restrictions.eq("password", Encrypter.getEncryptedPassword(password)));
        restrictions.add(logicAnd);
        List<Customer> managers = super.findByRestrictions(Customer.class, restrictions);
        return new ArrayList<>(managers);
    }

    @Override
    public Customer findById(Long id) {
        return super.findById(Customer.class, id);
    }

    @Override
    public Customer update(Customer customer) {
        return super.update(customer);
    }

}
