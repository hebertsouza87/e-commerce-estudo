package br.com.account.dao;

import br.com.account.dao.interf.Authenticate;
import br.com.account.dao.interf.ISellerDao;
import br.com.account.domain.Authenticatable;
import br.com.account.domain.Seller;
import br.com.commons.dao.AbstractDao;
import br.com.commons.security.Encrypter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class SellerDao extends AbstractDao<Seller> implements ISellerDao, Authenticate {

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Seller save(Seller seller) {
        return super.save(seller);
    }

    @Override
    public List<Seller> findByCodeOrNick(String codeOrNick) {
        if (StringUtils.isEmpty(codeOrNick)) {
            return null;
        }

        List<Criterion> restrictions = new ArrayList<>();

        LogicalExpression logicOr = Restrictions.or(Restrictions.eq("nickName", codeOrNick).ignoreCase(), Restrictions.eq("code", codeOrNick).ignoreCase());
        restrictions.add(logicOr);
        return super.findByRestrictions(Seller.class, restrictions);
    }

    @Override
    public List<Authenticatable> findByEmailAndPassword(String email, String password) {

        List<Criterion> restrictions = new ArrayList<>();

        LogicalExpression logicAnd = Restrictions.and(Restrictions.eq("email", email).ignoreCase(), Restrictions.eq("password", Encrypter.getEncryptedPassword(password)));
        restrictions.add(logicAnd);
        List<Seller> sellers = super.findByRestrictions(Seller.class, restrictions);
        return new ArrayList<>(sellers);
    }

    @Override
    public List<Seller> findByCpf(String cpf) {
        List<Criterion> restrictions = new ArrayList<>();

        restrictions.add(Restrictions.and(Restrictions.eq("cpf", cpf).ignoreCase()));
        List<Seller> sellers = super.findByRestrictions(Seller.class, restrictions);
        return new ArrayList<>(sellers);
    }
}
