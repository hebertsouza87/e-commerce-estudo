package br.com.account.tests.service;

import br.com.account.AbstractTest;
import br.com.account.dao.SellerDao;
import br.com.account.service.AuthenticateService;
import br.com.commons.exception.ForbidenException;
import br.com.commons.json.AuthenticateJson;
import br.com.commons.security.Authentication;
import br.com.commons.security.AuthenticationRole;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class AuthenticateServiceTest extends AbstractTest {

    @Autowired
    private AuthenticateService categoryService;

    @Autowired
    private SellerDao sellerDao;

    private AuthenticateJson authenticateJson;

    @Before

    public void setUp() {
        //sellerDao.createQuery("delete Seller").executeUpdate(); descomentar e criar o insert quando houver insert de vendedor.

        // Executar o comando abaixo no banco para criar um vendedor.
        // INSERT INTO `account_test`.`seller` (`CD_SELLER`, `NM_CODE`, `NM_CPF`, `NM_EMAIL`, `NM_NAME`, `NM_NICKNAME`, `NM_PASSWORD`) VALUES ('3', '123', '11122233344', 'email@teste.com', 'Teste Seller', 'Seller', 'ed2b1f468c5f915f3f1cf75d7068baae');
        authenticateJson = new AuthenticateJson();
        authenticateJson.setEmail("email@teste.com");
        authenticateJson.setPassword("12341234");
    }

    @Test
    public void authenticateSeller() {
        try {
            String authenticate = categoryService.authenticate(authenticateJson, AuthenticationRole.SELLER);
            Authentication authentication = Authentication.fromString(authenticate);
            Assert.assertEquals("3", authentication.getId());
        } catch (Exception ex) {
            Assert.fail("Não deveria lançar exception");
        }
    }

    @Test
    public void failToAuthenticateSeller() throws Exception {
        try {
            authenticateJson.setPassword("123123");
            String authenticate = categoryService.authenticate(authenticateJson, AuthenticationRole.SELLER);
            Authentication.fromString(authenticate);
            Assert.fail("Deveria lançar exception");
        } catch (ForbidenException ex) {
            Assert.assertTrue("Usuário/senha inválidos.".equals(ex.getMessage()));
        }
    }
}
