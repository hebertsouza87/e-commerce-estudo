package br.com.account.tests.validator;

import br.com.account.AbstractTest;
import br.com.account.validator.AuthenticateJsonValidator;
import br.com.commons.exception.UnprocessableEntityException;
import br.com.commons.json.AuthenticateJson;
import br.com.commons.validator.RestrictionType;
import br.com.commons.validator.ValidationError;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class AuthenticateJsonValidatorTest extends AbstractTest {

    @Autowired
    private AuthenticateJsonValidator authenticateJsonValidator;

    private AuthenticateJson authenticateJson;

    @Before
    public void setUp() {
        authenticateJson = new AuthenticateJson();
        authenticateJson.setEmail("teste@email.com");
        authenticateJson.setPassword("12341234");
    }

    @Test
    public void validauthenticateJson() {
        try {
            authenticateJsonValidator.validate(authenticateJson);
        } catch (UnprocessableEntityException ex) {
            fail("A entidade deveria ter sido validada");
        }
    }

    @Test
    public void invalidateNullAuthenticateJson() {
        try {
            authenticateJson = null;
            authenticateJsonValidator.validate(authenticateJson);
            fail("A entidade não deveria ter sido validada");
        } catch (UnprocessableEntityException ex) {

            assertEquals(1, ex.getErrors().size());

            final ValidationError error = ex.getErrors().get(0);

            assertTrue(error.getField().equals("authenticateJson"));
            assertTrue(error.getMessage().equals("Objeto nulo"));
            assertTrue(error.getRestrictionType().equals(RestrictionType.REQUIRED));
        }
    }

    @Test
    public void invalidateNullEmail() {
        try {
            authenticateJson.setEmail(null);
            authenticateJsonValidator.validate(authenticateJson);
            fail("A entidade não deveria ter sido validada");
        } catch (UnprocessableEntityException ex) {

            assertEquals(1, ex.getErrors().size());

            final ValidationError error = ex.getErrors().get(0);

            assertTrue(error.getField().equals("authenticateJson.email"));
            assertTrue(error.getMessage().equals("Obrigatório informar o e-mail"));
            assertTrue(error.getRestrictionType().equals(RestrictionType.REQUIRED));
        }
    }

    @Test
    public void invalidateEmptyEmail() {
        try {
            authenticateJson.setEmail("");
            authenticateJsonValidator.validate(authenticateJson);
            fail("A entidade não deveria ter sido validada");
        } catch (UnprocessableEntityException ex) {

            assertEquals(1, ex.getErrors().size());

            final ValidationError error = ex.getErrors().get(0);

            assertTrue(error.getField().equals("authenticateJson.email"));
            assertTrue(error.getMessage().equals("E-mail inválido"));
            assertTrue(error.getRestrictionType().equals(RestrictionType.INVALID));
        }
    }

    @Test
    public void invalidateInvalidEmail() {
        try {
            authenticateJson.setEmail("testeemail.com");
            authenticateJsonValidator.validate(authenticateJson);
            fail("A entidade não deveria ter sido validada");
        } catch (UnprocessableEntityException ex) {

            assertEquals(1, ex.getErrors().size());

            final ValidationError error = ex.getErrors().get(0);

            assertTrue(error.getField().equals("authenticateJson.email"));
            assertTrue(error.getMessage().equals("E-mail inválido"));
            assertTrue(error.getRestrictionType().equals(RestrictionType.INVALID));
        }
    }

    @Test
    public void invalidateNullPassword() {
        try {
            authenticateJson.setPassword(null);
            authenticateJsonValidator.validate(authenticateJson);
            fail("A entidade não deveria ter sido validada");
        } catch (UnprocessableEntityException ex) {

            assertEquals(1, ex.getErrors().size());

            final ValidationError error = ex.getErrors().get(0);

            assertTrue(error.getField().equals("authenticateJson.password"));
            assertTrue(error.getMessage().equals("Obrigatório informar a senha"));
            assertTrue(error.getRestrictionType().equals(RestrictionType.REQUIRED));
        }
    }

    @Test
    public void invalidateBigPassword() {
        try {
            authenticateJson.setPassword("123456789012345678901");
            authenticateJsonValidator.validate(authenticateJson);
            fail("A entidade não deveria ter sido validada");
        } catch (UnprocessableEntityException ex) {

            assertEquals(1, ex.getErrors().size());

            final ValidationError error = ex.getErrors().get(0);

            assertTrue(error.getField().equals("authenticateJson.password"));
            assertTrue(error.getMessage().equals("A senha deve conter entre 8 e 20 caracteres"));
            assertTrue(error.getRestrictionType().equals(RestrictionType.EXACT_LENGTH));
        }
    }

    @Test
    public void validatePasswordMinimunSize() {
        try {
            authenticateJson.setPassword("12341234");
            authenticateJsonValidator.validate(authenticateJson);
        } catch (UnprocessableEntityException ex) {
            fail("A entidade deveria ter sido validada");
        }
    }

}
