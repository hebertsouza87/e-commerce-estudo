package br.com.stock.dao;

import br.com.commons.dao.AbstractDao;
import br.com.stock.domain.Perfume;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class PerfumeDao extends AbstractDao<Perfume> {

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Perfume save(Perfume perfume) {
        return super.save(perfume);
    }

}
