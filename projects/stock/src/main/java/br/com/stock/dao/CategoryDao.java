package br.com.stock.dao;

import br.com.commons.dao.AbstractDao;
import br.com.stock.domain.Category;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class CategoryDao extends AbstractDao<Category> {

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Category save(Category category) {
        category.setId(null);
        return super.save(category);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Category update(Category category) {
        return super.update(category);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Category category) {
        super.delete(category);
    }

    public List<Category> findAll() {
        List<Criterion> restrictions = new ArrayList<>();

        return super.findAll(Category.class, Order.asc("name"), restrictions);
    }

    public Category findById(Long id) {
        return super.findById(Category.class, id);
    }

    public Category findByName(String category) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.like("name", category).ignoreCase());
        List<Category> categories = super.findByRestrictions(Category.class, restrictions);

        if (categories != null && categories.size() >= 1) {
            return categories.get(0);
        }

        return null;
    }

}
