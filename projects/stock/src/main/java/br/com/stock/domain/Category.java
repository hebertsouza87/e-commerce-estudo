package br.com.stock.domain;

import br.com.commons.json.CategoryJson;
import br.com.stock.model.CategoryLayoutEnum;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "CATEGORY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Category implements Serializable {

    private static final long serialVersionUID = 8746387L;

    @Id
    @SequenceGenerator(name = "category_id", sequenceName = "category_id_seg")
    @GeneratedValue(generator = "category_id", strategy = GenerationType.AUTO)
    @Column(name = "CD_CATEGORY")
    private Long id;

    @Column(name = "NM_NAME", nullable = false, unique = true, length = 255)
    private String name;

    @Column(name = "NM_URL_NAME", nullable = false)
    private String urlName;

    @Column(name = "URL_IMAGE", nullable = false)
    private String image;

    @Column(name = "DS_DESCRIPTION", nullable = false, columnDefinition = "VARCHAR(5000)")
    private String description;

    @Column(name = "LAYOUT", nullable = false)
    private CategoryLayoutEnum layout;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private List<Product> products = new ArrayList<>();

    public Category() {
    }

    public Category(CategoryJson categoryJson) {
        if (categoryJson != null) {
            this.id = categoryJson.getId();
            this.name = categoryJson.getName();
            this.description = categoryJson.getDescription();
            this.image = categoryJson.getImage();
            this.urlName = categoryJson.getUrlName();

            List<Product> productsList = new ArrayList<>();

            categoryJson.getProductsIds().stream().forEach((productId) -> {
                Product product = new Product();
                product.setId(productId);
                products.add(product);
            });

            this.products = productsList;
        }
    }

    public CategoryJson toJson() {
        CategoryJson categoryJson = new CategoryJson();
        categoryJson.setId(getId());
        categoryJson.setName(getName());
        categoryJson.setDescription(getDescription());
        categoryJson.setImage(getImage());
        categoryJson.setUrlName(getUrlName());

        List<Long> productsIds = new ArrayList<>();

        getProducts().stream().forEach((product) -> {
            productsIds.add(product.getId());
        });

        categoryJson.setProductsIds(null);

        return categoryJson;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public CategoryLayoutEnum getLayout() {
        return layout;
    }

    public void setLayout(CategoryLayoutEnum layout) {
        this.layout = layout;
    }
}
