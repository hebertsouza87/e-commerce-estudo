package br.com.stock.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "TYPE")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class Type implements Serializable{
    
    private static final long serialVersionUID = 19266512397124182L;

    @Id
    @SequenceGenerator(name = "type_id", sequenceName = "type_id_seg")
    @GeneratedValue(generator = "type_id", strategy = GenerationType.AUTO)
    @Column(name = "CD_TYPE")
    private Long id;

    @Column(name = "TYPE", nullable = false, length = 20)
    private String type;

    public Type() {
    }

    public Type(Long id, String type) {
        this.id = id;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
}
