package br.com.stock.service;

import br.com.commons.exception.ConflictException;
import br.com.commons.exception.NotFoundException;
import br.com.commons.json.ProductMovJson;
import br.com.stock.dao.ProductDao;
import br.com.stock.domain.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductService {

    static final Logger LOG = Logger.getLogger(ProductService.class);

    @Autowired
    private ProductDao productDao;

    @Autowired
    private CategoryService categoryService;

    public Product create(Product product) throws Exception {
        if (productDao.findByCodeOrName(product.getName(), product.getCode()).size() > 0) {
            throw new ConflictException();
        } else {
            product.setCategory(categoryService.findById(product.getCategory().getId()));
            product.setSession(0);
            product.setOrder(0);
            product.setSold(0);
            product.setStock(product.getAvailable());
            return productDao.save(product);
        }
    }

    public List<Product> findAll() {
        return productDao.findAll();
    }

    public List<Product> findByCategoryId(Long categoryId) {
        return productDao.findByCategoryId(categoryId);
    }

    public Product findById(Long productId) throws Exception {
        Product product = productDao.findById(productId);

        if (product == null) {
            throw new NotFoundException();
        }
        return product;
    }

    public Product findByName(String productName) throws Exception {
        Product product = productDao.findByName(productName);

        if (product == null) {
            throw new NotFoundException();
        }
        return product;
    }

    public Integer stockToSession(ProductMovJson productMovJson) {
        try {
            Integer quant = 0;
            Product prod = this.findById(productMovJson.getId());
            if (prod != null) {
                if (prod.getAvailable() >= productMovJson.getQuantity()) {
                    quant = productMovJson.getQuantity();
                } else {
                    quant = prod.getAvailable();
                }

                prod.setAvailable(prod.getAvailable() - quant);
                prod.setSession(prod.getSession() + quant);
                productDao.update(prod);
            }

            return quant;

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public Integer sessionToOrder(ProductMovJson productMovJson) {
        try {
            Integer quant = productMovJson.getQuantity();
            Product prod = this.findById(productMovJson.getId());
            if (prod != null) {
                prod.setSession(prod.getSession() - quant);
                prod.setOrder(prod.getOrder() + quant);
                productDao.update(prod);
            }

            return quant;

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public Integer orderToSold(ProductMovJson productMovJson) {
        try {
            Integer quant = productMovJson.getQuantity();
            Product prod = this.findById(productMovJson.getId());
            if (prod != null) {
                prod.setOrder(prod.getOrder() - quant);
                prod.setSold(prod.getSold() + quant);
                productDao.update(prod);
            }
            return quant;

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public void remove(Long productId) throws Exception {
        final Product product = productDao.findById(productId);
        if (product == null) {
            throw new NotFoundException();
        }

        productDao.delete(product);
    }

    public void verifyStockProducts() {
        //Criar o array list
        ArrayList<Product> products = (ArrayList<Product>) findAll();
        for (Product product : products) {
            Integer stock = product.getStock();
            Integer sum = product.getAvailable() + product.getOrder() + product.getSession();
            if (!stock.equals(sum)) {
                LOG.warn("Produto: " + product.getName() + " está com o estoque incorreto!");
            }
        }
    }

}
