package br.com.stock.controller;

import br.com.stock.domain.Category;
import br.com.stock.domain.Product;
import br.com.stock.model.CategoryLayoutEnum;
import br.com.stock.service.CategoryService;
import br.com.stock.service.ProductService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/init")
public class InitController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> init(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Category category1 = new Category();
        category1.setName("Perfume");
        category1.setUrlName("perfume");
        category1.setLayout(CategoryLayoutEnum.NO_CATEGORY);
        category1.setDescription("Descrição de teste para a categoria de perfumes.");
        category1.setImage("http://1.bp.blogspot.com/_6sQmhvKIghQ/RxljuQ_w7GI/AAAAAAAAAfg/-Bhzp-wzkM8/s400/flores-magnolia_1082_1.jpg");

        Category category2 = new Category();
        category2.setName("hidratante");
        category2.setUrlName("hidratante");
        category2.setLayout(CategoryLayoutEnum.GENERAL);
        category2.setDescription("Descrição de teste para a categoria de Hidratante.");
        category2.setImage("http://1.bp.blogspot.com/_6sQmhvKIghQ/RxljuQ_w7GI/AAAAAAAAAfg/-Bhzp-wzkM8/s400/flores-magnolia_1082_1.jpg");

        Product product1 = new Product();
        product1.setCategory(category1);
        product1.setCode("M01");
        product1.setName("Azarro");
        product1.setDescription("Descrição completa do perfume azarro");
        product1.setPrice(BigDecimal.valueOf(35.50));
        product1.setAvailable(100);
        product1.setWeight(100L);
        product1.setImageUrl("");
        product1.setShotDescription("Descrição curta do perfume");

        Product product2 = new Product();
        product2.setCategory(category1);
        product2.setCode("M02");
        product2.setName("Ferrari Black");
        product2.setDescription("Descrição completa do perfume Ferrari Black");
        product2.setPrice(BigDecimal.valueOf(37.50));
        product2.setAvailable(100);
        product2.setWeight(100L);
        product2.setImageUrl("");
        product2.setShotDescription("Descrição curta do perfume");

        List<Product> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        category1.setProducts(products);

        category1 = categoryService.create(category1);
        category2 = categoryService.create(category2);
//        productService.create(product1);
//        productService.create(product2);

        List<Object> list = new ArrayList<>();
        list.add(category1.toJson());
        list.add(category2.toJson());
        list.add(product1.toJson());
        list.add(product2.toJson());

        return new ResponseEntity<>(list.toString(), HttpStatus.CREATED);
    }
}
