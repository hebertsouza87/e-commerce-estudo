package br.com.stock.service;

import br.com.commons.exception.ConflictException;
import br.com.stock.dao.PerfumeDao;
import br.com.stock.dao.ProductDao;
import br.com.stock.domain.Perfume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PerfumeService {

    @Autowired
    private PerfumeDao perfumeDao;
    
    @Autowired
    private CategoryService categoryService;
    
    @Autowired
    private TypeService typeService;
    
    @Autowired
    private ProductDao productDao;

    public Perfume create(Perfume perfume) throws Exception {       
        if (productDao.findByCodeOrName(perfume.getName(), perfume.getCode()).size() > 0) { //Valida para não cadastra um perfume que ja exista
            throw new ConflictException();
        } else {
            perfume.setId(null);
            perfume.setCategory(categoryService.findById(perfume.getCategory().getId()));
            perfume.setSession(0);
            perfume.setOrder(0);
            perfume.setSold(0);
            perfume.setStock(perfume.getAvailable());
            perfume.setType(typeService.findById(perfume.getType().getId()));
        }
        return perfumeDao.save(perfume);
    }
}
