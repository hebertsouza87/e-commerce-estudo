package br.com.stock.service;

import br.com.commons.exception.NotFoundException;
import br.com.stock.dao.TypeDao;
import br.com.stock.domain.Type;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class TypeService {

    static final Logger LOG = Logger.getLogger(TypeService.class);

    @Autowired
    private TypeDao typeDao;

    public Type create(Type type) throws Exception {
//        if (typeDao.findByName(type.getName()) != null) {
//            throw new ConflictException();
//        } else {
            return typeDao.save(type);
//        }
    }
    
    public Type findById(Long typeId) throws NotFoundException {
        Type type = typeDao.findById(typeId);

        if (type == null) {
            throw new NotFoundException();
        }
        return type;
    }
}
