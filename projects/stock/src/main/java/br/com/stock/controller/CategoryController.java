package br.com.stock.controller;

import br.com.commons.json.CategoryJson;
import br.com.commons.json.CategoryListJson;
import br.com.stock.domain.Category;
import br.com.stock.service.CategoryService;
import br.com.stock.validator.category.CategoryValidator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryValidator categoryValidator;

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<CategoryJson> create(@RequestBody CategoryJson categoryJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        categoryValidator.validate(categoryJson);
        Category category = new Category(categoryJson);
        category = categoryService.create(category);

        return new ResponseEntity<>(category.toJson(), HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<CategoryListJson> findAll(HttpServletRequest request, HttpServletResponse response) throws Exception {
        final List<Category> categories = categoryService.findAll();
        final CategoryListJson categoryListJson = new CategoryListJson();

        if (categories != null && !categories.isEmpty()) {
            for (Category category : categories) {
                categoryListJson.add(category.toJson());
            }
        }

        return new ResponseEntity<>(categoryListJson, HttpStatus.OK);
    }

    @RequestMapping(value = "/{categoryId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<CategoryJson> findById(@PathVariable("categoryId") Long categoryId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        final Category category = categoryService.findById(categoryId);

        return new ResponseEntity<>(category.toJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{categoryId}", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity<CategoryJson> deleteById(@PathVariable("categoryId") Long categoryId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        categoryService.remove(categoryId);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
