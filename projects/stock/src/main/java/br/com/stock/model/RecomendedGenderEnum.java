package br.com.stock.model;

import org.springframework.util.StringUtils;

public enum RecomendedGenderEnum {

    MALE("male"),
    FEMALE("female"),
    UNISEX("unisex");

    private String gender;

    RecomendedGenderEnum(String gender) {
        this.setGender(gender);
    }

    public static RecomendedGenderEnum getByString(String gender) {
        if (StringUtils.isEmpty(gender)) {
            return null;
        }

        for (RecomendedGenderEnum genderEnum : RecomendedGenderEnum.values()) {
            if (gender.equals(genderEnum.getGender())) {
                return genderEnum;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.name();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}
