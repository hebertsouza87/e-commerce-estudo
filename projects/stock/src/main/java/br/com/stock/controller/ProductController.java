package br.com.stock.controller;

import br.com.commons.json.ProductJson;
import br.com.commons.json.ProductListJson;
import br.com.commons.json.ProductMovJson;
import br.com.stock.domain.Product;
import br.com.stock.service.ProductService;
import br.com.stock.validator.category.ProductValidator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductValidator productValidator;

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<ProductJson> create(@RequestBody ProductJson productJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        productValidator.validate(productJson);
        Product product = new Product(productJson);
        product = productService.create(product);

        return new ResponseEntity<>(product.toJson(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/stockToSession", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<ProductMovJson> stockToSession(@RequestBody ProductMovJson productMovJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer quant = productService.stockToSession(productMovJson);

        productMovJson.setQuantity(quant);

        return new ResponseEntity<>(productMovJson, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/sessionToOrder", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<ProductMovJson> sessionToOrder(@RequestBody ProductMovJson productMovJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer quant = productService.sessionToOrder(productMovJson);

        productMovJson.setQuantity(quant);

        return new ResponseEntity<>(productMovJson, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/orderToSold", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<ProductMovJson> orderToSold(@RequestBody ProductMovJson productMovJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer quant = productService.orderToSold(productMovJson);

        productMovJson.setQuantity(quant);

        return new ResponseEntity<>(productMovJson, HttpStatus.ACCEPTED);
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<ProductListJson> findAll(HttpServletRequest request, HttpServletResponse response) throws Exception {
        final List<Product> products = productService.findAll();
        final ProductListJson categoryListJson = new ProductListJson();

        if (products != null && !products.isEmpty()) {
            for (Product product : products) {
                categoryListJson.add(product.toJson());
            }
        }

        return new ResponseEntity<>(categoryListJson, HttpStatus.OK);
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<ProductJson> findById(@PathVariable("productId") Long productId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        final Product product = productService.findById(productId);

        return new ResponseEntity<>(product.toJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "name/{productName}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<ProductJson> findByName(@PathVariable("productName") String productName, HttpServletRequest request, HttpServletResponse response) throws Exception {
        final Product product = productService.findByName(productName);

        return new ResponseEntity<>(product.toJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "category/{categoryId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<ProductListJson> findByCategoryId(@PathVariable("categoryId") Long categoryId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        final List<Product> products = productService.findByCategoryId(categoryId);
        final ProductListJson categoryListJson = new ProductListJson();

        if (products != null && !products.isEmpty()) {
            for (Product product : products) {
                categoryListJson.add(product.toJson());
            }
        }

        return new ResponseEntity<>(categoryListJson, HttpStatus.OK);
    }
}
