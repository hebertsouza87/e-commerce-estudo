package br.com.stock.domain;

import br.com.commons.json.ProductJson;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Check;

@Entity
@Table(name = "PRODUCT")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.NONE)
@Check(constraints = "NR_WEIGHT >= 0 and NR_PRICE >= 0")
public class Product implements Serializable {

    private static final long serialVersionUID = 13472364236419L;

    @Id
    @SequenceGenerator(name = "product_id", sequenceName = "product_id_seg")
    @GeneratedValue(generator = "product_id", strategy = GenerationType.AUTO)
    @Column(name = "CD_PRODUCT")
    private Long id;

    @Column(name = "NM_NAME", nullable = false)
    private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CD_CATEGORY", nullable = false)
    private Category category;

    @Column(name = "NM_IMAGE_URL", nullable = false)
    private String imageUrl;

    @Column(name = "NR_PRICE", precision = 5, scale = 2)
    private BigDecimal price = BigDecimal.ZERO;

    @Column(name = "NM_DESCRIPTION", length = 9000)
    private String description;

    @Column(name = "NM_SHOT_DESCRIPTION", length = 255)
    private String shotDescription;

    @Column(name = "NM_CODE", length = 20)
    private String code;

    @Column(name = "NR_WEIGHT", columnDefinition = "int default 0")
    private Long weight = 0L;

    @Column(name = "NR_AVAILABLE", columnDefinition = "int default 0")
    private Integer available = 0;

    @Column(name = "NR_SESSION", columnDefinition = "int default 0")
    private Integer session = 0;

    @Column(name = "NR_ORDER", columnDefinition = "int default 0")
    private Integer order = 0;

    @Column(name = "NR_SOLD", columnDefinition = "int default 0")
    private Integer sold = 0;
    
    @Column(name = "NR_STOCK", columnDefinition = "int default 0")
    private Integer stock = 0;        

    public Product() {

    }

    public Product(ProductJson productJson) {
        if (productJson != null) {
            this.id = productJson.getId();
            this.name = productJson.getName();

            Category category = new Category();
            category.setId(productJson.getCategoryId());
            this.category = category;

            this.imageUrl = productJson.getImageUrl();
            this.price = productJson.getPrice();
            this.description = productJson.getDescription();
            this.shotDescription = productJson.getShotDescription();
            this.code = productJson.getCode();
            this.weight = productJson.getWeight();
            this.available = productJson.getAvailable();
            this.session = productJson.getSession();
            this.order = productJson.getOrder();
            this.sold = productJson.getSold();
            this.stock = productJson.getStock();
        }
    }

    public ProductJson toJson() {
        ProductJson productJson = new ProductJson();
        productJson.setId(getId());
        productJson.setName(getName());
        productJson.setCategoryId(getCategoryId());
        productJson.setImageUrl(getImageUrl());
        productJson.setPrice(getPrice());
        productJson.setDescription(getDescription());
        productJson.setShotDescription(getShotDescription());
        productJson.setCode(getCode());
        productJson.setWeight(getWeight());
        productJson.setAvailable(getAvailable());
        productJson.setSession(getSession());
        productJson.setOrder(getOrder());
        productJson.setSold(getSold());
        productJson.setStock(getStock());

        return productJson;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public Long getCategoryId() {
        return category.getId();
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShotDescription() {
        return shotDescription;
    }

    public void setShotDescription(String shotDescription) {
        this.shotDescription = shotDescription;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public Integer getSession() {
        return session;
    }

    public void setSession(Integer session) {
        this.session = session;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getSold() {
        return sold;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
        
}
