package br.com.stock.controller;

import br.com.commons.json.PerfumeJson;
import br.com.stock.domain.Perfume;
import br.com.stock.service.PerfumeService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/perfume")
public class PerfumeController {

    @Autowired
    private PerfumeService perfumeService;

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<PerfumeJson> create(@RequestBody PerfumeJson perfumeJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Perfume perfume = new Perfume(perfumeJson);
        perfume = perfumeService.create(perfume);       
        
        return new ResponseEntity<>(perfume.toJson(), HttpStatus.CREATED);
    }

}
