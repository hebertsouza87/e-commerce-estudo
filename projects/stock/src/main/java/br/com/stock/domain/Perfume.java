package br.com.stock.domain;

import br.com.commons.json.PerfumeJson;
import br.com.stock.model.IntensityEnum;
import br.com.stock.model.RecomendedGenderEnum;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "PERFUME")
@PrimaryKeyJoinColumn(name = "CD_PRODUCT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Perfume extends Product implements Serializable{
    
    private static final long serialVersionUID = 13126512392018432L;
    
    @Column(name = "GENDER", nullable = false, columnDefinition = "varchar(15)")
    @Enumerated(EnumType.STRING)
    private RecomendedGenderEnum recomendedGender = RecomendedGenderEnum.FEMALE;
    
    @Column(name = "INSPIRATION", nullable = false, length = 30)
    private String inspiration;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CD_TYPE", nullable = false)
    private Type type;
    
    @Column(name = "INTENSITY", nullable = false, columnDefinition = "varchar(15)")
    @Enumerated(EnumType.STRING)
    private IntensityEnum intensity = IntensityEnum.SOFT;
    
    @Column(name = "SIZE", precision = 5, scale = 2)
    private BigDecimal size;

    public Perfume() {
    }
       
    public Perfume(PerfumeJson perfumeJson){
        if (perfumeJson != null){            
            /*Product*/
            setId(perfumeJson.getId());
            setName(perfumeJson.getName());
            Category category = new Category();
            category.setId(perfumeJson.getCategoryId());
            setCategory(category);
            setImageUrl(perfumeJson.getImageUrl());            
            setPrice(perfumeJson.getPrice());
            setDescription(perfumeJson.getDescription());
            setShotDescription(perfumeJson.getShotDescription());
            setCode(perfumeJson.getCode());
            setWeight(perfumeJson.getWeight());
            setAvailable(perfumeJson.getAvailable());
            setSession(perfumeJson.getSession());
            setOrder(perfumeJson.getOrder());
            setSold(perfumeJson.getSold());
            setStock(perfumeJson.getStock());
            /*Perfume*/
            setRecomendedGender(RecomendedGenderEnum.getByString(perfumeJson.getRecomendedGender()));
            setInspiration(perfumeJson.getInspiration());
            Type type = new Type();
            type.setId(perfumeJson.getTypeId());
            setType(type);
            setIntensity(IntensityEnum.getByString(perfumeJson.getIntensity()));
            setSize(perfumeJson.getSize());                       
        }
    }
    
    public PerfumeJson toJson(){
        PerfumeJson perfumeJson = new PerfumeJson(super.toJson());
        perfumeJson.setRecomendedGender(getRecomendedGender().getGender());
        perfumeJson.setInspiration(getInspiration());
        perfumeJson.setTypeId(getType().getId());
        perfumeJson.setIntensity(getIntensity().getIntensity());
        perfumeJson.setSize(getSize());        
        return perfumeJson;
    }

    public RecomendedGenderEnum getRecomendedGender() {
        return recomendedGender;
    }

    public void setRecomendedGender(RecomendedGenderEnum recomendedGender) {
        this.recomendedGender = recomendedGender;
    }

    public String getInspiration() {
        return inspiration;
    }

    public void setInspiration(String inspiration) {
        this.inspiration = inspiration;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public IntensityEnum getIntensity() {
        return intensity;
    }

    public void setIntensity(IntensityEnum intensity) {
        this.intensity = intensity;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }
                
}
