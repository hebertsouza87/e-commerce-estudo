package br.com.stock.validator.category;

import br.com.commons.json.CategoryJson;
import br.com.commons.validator.AbstractValidator;
import br.com.commons.validator.RestrictionType;

public class CategoryValidator extends AbstractValidator<CategoryJson> {

    @Override
    protected void apply(CategoryJson categoryJson) {
        if (categoryJson == null) {
            setInvalid(true).setError("categoryJson", RestrictionType.REQUIRED, "Objeto nulo");
            return;
        }

        isNull(categoryJson.getName()).setError("categoryJson.name", RestrictionType.REQUIRED, "Obrigatório informar o nome da categoria");

        if (categoryJson.getName() != null) {
            isBlank(categoryJson.getName()).setError("categoryJson.name", RestrictionType.REQUIRED, "Nome da categoria não pode estar em branco");
            isLengthBetween(categoryJson.getName(), 3, 20).setError("categoryJson.name", RestrictionType.EXACT_LENGTH, "Nome da categoria deve conter entre 3 e 20 caracteres");
        }
    }
}
