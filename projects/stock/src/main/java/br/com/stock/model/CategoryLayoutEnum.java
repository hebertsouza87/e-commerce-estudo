package br.com.stock.model;

public enum CategoryLayoutEnum {

    GENERAL("general"),
    NO_CATEGORY("noCategory");

    private String name;

    CategoryLayoutEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
