package br.com.stock.config;

import br.com.commons.appConfig.ConfigEnumInterface;

public enum ConfigEnum implements ConfigEnumInterface<ConfigEnum> {

    ENDPOINT(1L, "endpoint", "http://localhost:8082/stock/"),
    TIME_TO_VALIDATE_STOCK(2L, "TIME_TO_VALIDATE_STOCK", "720");

    private final Long id;
    private final String name;
    private final String defaultValue;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    private ConfigEnum(Long id, String key, String defaultValue) {
        this.id = id;
        this.name = key;
        this.defaultValue = defaultValue;
    }

    @Override
    public ConfigEnum[] getValues() {
        return values();
    }
}
