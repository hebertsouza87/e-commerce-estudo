package br.com.stock.schedule;

import br.com.commons.appConfig.AppConfig;
import br.com.commons.schedule.AbstractScheduleManager;
import br.com.stock.config.ConfigEnum;
import br.com.stock.schedule.task.StockTask;
import br.com.stock.service.ProductService;
import javax.annotation.PostConstruct;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class StockScheduleManager extends AbstractScheduleManager {

    private static final Log LOG = LogFactory.getLog(StockScheduleManager.class);
    private StockTask stockTask;

    private final ProductService productService;
    private final AppConfig appConfig;

    public StockScheduleManager(ProductService productService, AppConfig appConfig) {
        this.productService = productService;
        this.appConfig = appConfig;

        schedule();
    }

    @Override
    @PostConstruct
    public void schedule() {
        scheduleStock();
    }

    private void scheduleStock() {
        cancelSchedule(stockTask);
        stockTask = new StockTask(productService);
        scheduleTask(stockTask, appConfig.getValueLongDefault(ConfigEnum.TIME_TO_VALIDATE_STOCK, 1L), appConfig.getValueLongDefault(ConfigEnum.TIME_TO_VALIDATE_STOCK, 720L));
    }
}
