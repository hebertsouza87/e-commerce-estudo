package br.com.stock.validator.category;

import br.com.commons.json.ProductJson;
import br.com.commons.validator.AbstractValidator;
import br.com.commons.validator.RestrictionType;
import br.com.stock.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductValidator extends AbstractValidator<ProductJson> {

    @Autowired
    private CategoryService categoryService;

    @Override
    protected void apply(ProductJson productJson) {
        if (productJson == null) {
            setInvalid(true).setError("productJson", RestrictionType.REQUIRED, "Objeto nulo");
            return;
        }
        /*NESSE CASO FAZ A VALIDAÇÃO ANTES DE SETAR A CATEGORIA
         isNull(productJson.getCategoryId()).setError("productJson.categoryId", RestrictionType.REQUIRED, "Obrigatório informar a categoria do produto");
         if (productJson.getCategoryId() != null) {
         try {
         categoryService.findById(productJson.getCategoryId());
         } catch (NotFoundException ex) {
         setInvalid(true).setError("productJson.categoryId", RestrictionType.INVALID, "Categoria inválida");
         }
         }
         */

        isNull(productJson.getName()).setError("productJson.name", RestrictionType.REQUIRED, "Obrigatório informar o nome do produto");
        if (productJson.getName() != null) {
            isBlank(productJson.getName()).setError("productJson.name", RestrictionType.REQUIRED, "Nome do produto não pode estar em branco");
            isLengthBetween(productJson.getName(), 3, 20).setError("productJson.name", RestrictionType.EXACT_LENGTH, "Nome do produto deve conter entre 3 e 20 caracteres");
        }

        isNull(productJson.getImageUrl()).setError("productJson.imageUrl", RestrictionType.REQUIRED, "Obrigatório informar uma imagem para o produto");
        if (productJson.getImageUrl() != null) {
            isBlank(productJson.getImageUrl()).setError("productJson.imageUrl", RestrictionType.REQUIRED, "Caminho da imagem não pode estar em branco");
        }

        if (productJson.getDescription() != null) {
            isLengthBetween(productJson.getDescription(), 0, 9000).setError("productJson.description", RestrictionType.EXACT_LENGTH, "A descrição do produto deve conter no máximo 9000 caracteres");
        }

        if (productJson.getShotDescription() != null) {
            isLengthBetween(productJson.getShotDescription(), 0, 255).setError("productJson.shotDescription", RestrictionType.EXACT_LENGTH, "A pequena descrição do produto deve conter no máximo 255 caracteres");
        }

        if (productJson.getCode() != null) {
            isLengthBetween(productJson.getCode(), 0, 20).setError("productJson.code", RestrictionType.EXACT_LENGTH, "O código do produto deve conter no máximo 20 caracteres");
        }

    }
}
