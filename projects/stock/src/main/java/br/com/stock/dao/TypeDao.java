package br.com.stock.dao;

import br.com.commons.dao.AbstractDao;
import br.com.stock.domain.Type;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class TypeDao extends AbstractDao<Type> {

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Type save(Type type) {
        type.setId(null);
        return super.save(type);
    }
    
    public Type findById(Long id) {
        return super.findById(Type.class, id);
    }
   
}
