package br.com.stock.schedule.task;

import br.com.stock.service.ProductService;
import java.util.TimerTask;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class StockTask extends TimerTask {

    private static final Log LOG = LogFactory.getLog(StockTask.class);

    private final ProductService productService;

    public StockTask(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void run() {
        try {
            productService.verifyStockProducts();
        } catch (Exception e) {
            LOG.error("Ocorreu um erro ao verificar o estoque dos produtos", e);
        }
    }
}
