package br.com.stock.dao;

import br.com.commons.dao.AbstractDao;
import br.com.stock.domain.Product;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED)
public class ProductDao extends AbstractDao<Product> {

    @Override
    public Product save(Product product) {
        product.setId(null);
        return super.save(product);
    }

    @Override    
    public Product update(Product product) {
        return super.update(product);
    }

    @Override    
    public void delete(Product product) {
        super.delete(product);
    }

    public Product findById(Long id) {
        return super.findById(Product.class, id);
    }

    public List<Product> findByCategoryId(Long categoryId) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("category.id", categoryId));
        List<Product> products = super.findByRestrictions(Product.class, restrictions);

        return products;
    }

    public Product findByName(String product) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.like("name", product).ignoreCase());
        List<Product> categories = super.findByRestrictions(Product.class, restrictions);

        if (categories != null && categories.size() >= 1) {
            return categories.get(0);
        }

        return null;
    }

    public List<Product> findByCodeOrName(String name, String code) {

        List<Criterion> restrictions = new ArrayList<>();

        LogicalExpression logicOr = Restrictions.or(Restrictions.eq("name", name).ignoreCase(), Restrictions.eq("code", code).ignoreCase());
        restrictions.add(logicOr);
        return super.findByRestrictions(Product.class, restrictions);

    }

    public List<Product> findAll() {
        List<Criterion> restrictions = new ArrayList<>();
        return super.findAll(Product.class, Order.asc("name"), restrictions);
    }
}
