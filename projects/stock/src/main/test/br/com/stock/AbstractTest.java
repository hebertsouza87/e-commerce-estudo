package br.com.stock;

import org.junit.Ignore;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Ignore //Somente para não gerar error de classe sem teste ao executar todos os testes do projeto.
@ContextConfiguration({"resources/spring/testDataSource.xml", "resources/spring/testApplicationContext.xml"})
@Transactional(propagation = Propagation.REQUIRED)
public abstract class AbstractTest extends AbstractTransactionalJUnit4SpringContextTests {

}
