package br.com.stock.tests.service;

import br.com.commons.exception.ConflictException;
import br.com.commons.exception.NotFoundException;
import br.com.stock.AbstractTest;
import br.com.stock.dao.CategoryDao;
import br.com.stock.dao.ProductDao;
import br.com.stock.domain.Category;
import br.com.stock.domain.Product;
import br.com.stock.service.ProductService;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductServiceTest extends AbstractTest {

    private Category category;
    private Product product;
    private Product product2;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private CategoryDao categoryDao;

    @Before
    public void setUp() throws Exception {
        // Limpa o banco para a execução dos testes
        productDao.createQuery("delete Product").executeUpdate();
        productDao.createQuery("delete Category").executeUpdate();

        category = new Category();
        category.setName("Category Test");
        category.setDescription("Description category teste");
        category.setImage("C:\\teste.jpg");
        category.setUrlName("Teste");

        Category savedCategory = categoryDao.save(category);

        category.setId(savedCategory.getId()); // Feito desta forma para que a categoria não seja um objeto do banco, o que invalidaria os nossos testes

        product = new Product();
        product.setName("Product Test");
        product.setCategory(category);
        product.setImageUrl("C:\\Imagens\\Teste");
        product.setDescription("product description");
        product.setCode("123");

        product2 = new Product();
        product2.setName("Product Test 2");
        product2.setCategory(category);
        product2.setImageUrl("C:\\Imagens\\Teste");
        product2.setDescription("product2 description");
        product.setCode("456");
    }

    @Test
    public void create() {
        try {
            Product createdProduct = productService.create(product);
            Product productFound = productDao.findById(createdProduct.getId());

            Assert.assertEquals(product.getName(), productFound.getName());
        } catch (Exception ex) {
            Assert.fail("Não deveria lançar exception");
        }
    }

    @Test
    public void productAlreadyExistsName() throws Exception {
        try {
            productDao.save(product);

            product.setCode("321"); // Trocando o código para gerar conflito somente com o nome

            productService.create(product);
            Assert.fail("Deveria lançar ConflictException");
        } catch (ConflictException ex) {
        }
    }

    @Test
    public void productAlreadyExistsCode() throws Exception {
        try {
            productDao.save(product);

            product.setName("Product Test 3"); // Trocando o nome para gerar conflito somente com o código

            productService.create(product);
            Assert.fail("Deveria lançar ConflictException");
        } catch (ConflictException ex) {
        }
    }

    @Test
    public void productRemove() throws Exception {
        Product savedProduct = productDao.save(product);

        productService.remove(savedProduct.getId());

        Assert.assertNull(productDao.findById(savedProduct.getId()));

    }

    @Test
    public void productNotFoundToRemove() throws Exception {
        try {
            productService.remove(0L);
            Assert.fail("Deveria lançar NotFoundException");
        } catch (NotFoundException e) {
        }
    }

    @Test
    public void findAll() throws Exception {
        productDao.save(product);
        productDao.save(product2);

        List<Product> products = productService.findAll();

        Assert.assertEquals(2, products.size());
        Assert.assertEquals("Product Test", products.get(0).getName());
        Assert.assertEquals("Product Test 2", products.get(1).getName());
    }

    @Test
    public void findById() throws Exception {
        Product savedProduct = productDao.save(product);

        Product productFound = productService.findById(savedProduct.getId());

        Assert.assertEquals(product.getName(), productFound.getName());
    }

    @Test
    public void findByCategoryId() throws Exception {
        productDao.save(product);
        productDao.save(product2);

        List<Product> products = productService.findByCategoryId(category.getId());

        Assert.assertEquals(2, products.size());
        Assert.assertEquals("Product Test", products.get(0).getName());
        Assert.assertEquals("Product Test 2", products.get(1).getName());
    }

    @Test
    public void findByIdNotFound() throws Exception {
        try {
            productService.findById(10L);
            Assert.fail("Deveria lançar NotFoundException");
        } catch (NotFoundException e) {
        }
    }

}
