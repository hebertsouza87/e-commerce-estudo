package br.com.stock.tests.validator;

import br.com.commons.exception.UnprocessableEntityException;
import br.com.commons.json.CategoryJson;
import br.com.commons.validator.RestrictionType;
import br.com.commons.validator.ValidationError;
import br.com.stock.AbstractTest;
import br.com.stock.validator.category.CategoryValidator;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CategoryJsonValidatorTest extends AbstractTest {

    @Autowired
    private CategoryValidator categoryValidator;

    private CategoryJson categoryJson;

    @Before
    public void setUp() {
        categoryJson = new CategoryJson();
        categoryJson.setId(1L);
        categoryJson.setName("Name Test");
    }

    @Test
    public void validCategory() {
        try {
            categoryValidator.validate(categoryJson);
        } catch (UnprocessableEntityException ex) {
            fail("A entidade deveria ter sido validada");
        }
    }

    @Test
    public void invalidateNullCategoryJson() {
        try {
            categoryJson = null;
            categoryValidator.validate(categoryJson);
            fail("A entidade não deveria ter sido validada");
        } catch (UnprocessableEntityException ex) {

            assertEquals(1, ex.getErrors().size());

            final ValidationError error = ex.getErrors().get(0);

            assertTrue(error.getField().equals("categoryJson"));
            assertTrue(error.getMessage().equals("Objeto nulo"));
            assertTrue(error.getRestrictionType().equals(RestrictionType.REQUIRED));
        }
    }

    @Test
    public void invalidateNullName() {
        try {
            categoryJson.setName(null);
            categoryValidator.validate(categoryJson);
            fail("A entidade não deveria ter sido validada");
        } catch (UnprocessableEntityException ex) {

            assertEquals(1, ex.getErrors().size());

            final ValidationError error = ex.getErrors().get(0);

            assertTrue(error.getField().equals("categoryJson.name"));
            assertTrue(error.getMessage().equals("Obrigatório informar o nome da categoria"));
            assertTrue(error.getRestrictionType().equals(RestrictionType.REQUIRED));
        }
    }

    @Test
    public void invalidateEmptyName() {
        try {
            categoryJson.setName("");
            categoryValidator.validate(categoryJson);
            fail("A entidade não deveria ter sido validada");
        } catch (UnprocessableEntityException ex) {

            assertEquals(1, ex.getErrors().size());

            final ValidationError error = ex.getErrors().get(0);

            assertTrue(error.getField().equals("categoryJson.name"));
            assertTrue(error.getMessage().equals("Nome da categoria não pode estar em branco"));
            assertTrue(error.getRestrictionType().equals(RestrictionType.REQUIRED));
        }
    }

    @Test
    public void invalidateShotName() {
        try {
            categoryJson.setName("12");
            categoryValidator.validate(categoryJson);
            fail("A entidade não deveria ter sido validada");
        } catch (UnprocessableEntityException ex) {

            assertEquals(1, ex.getErrors().size());

            final ValidationError error = ex.getErrors().get(0);

            assertTrue(error.getField().equals("categoryJson.name"));
            assertTrue(error.getMessage().equals("Nome da categoria deve conter entre 3 e 20 caracteres"));
            assertTrue(error.getRestrictionType().equals(RestrictionType.EXACT_LENGTH));
        }
    }

    @Test
    public void invalidateBigName() {
        try {
            categoryJson.setName("123456789012345678901");
            categoryValidator.validate(categoryJson);
            fail("A entidade não deveria ter sido validada");
        } catch (UnprocessableEntityException ex) {

            assertEquals(1, ex.getErrors().size());

            final ValidationError error = ex.getErrors().get(0);

            assertTrue(error.getField().equals("categoryJson.name"));
            assertTrue(error.getMessage().equals("Nome da categoria deve conter entre 3 e 20 caracteres"));
            assertTrue(error.getRestrictionType().equals(RestrictionType.EXACT_LENGTH));
        }
    }

    @Test
    public void validateMinimunSize() {
        try {
            categoryJson.setName("123");
            categoryValidator.validate(categoryJson);
        } catch (UnprocessableEntityException ex) {
            fail("A entidade deveria ter sido validada");
        }
    }

    @Test
    public void validateMaximumSize() {
        try {
            categoryJson.setName("12345678901234567890");
            categoryValidator.validate(categoryJson);
        } catch (UnprocessableEntityException ex) {
            fail("A entidade deveria ter sido validada");
        }
    }
}
