CREATE TABLE stock.product_move_to
(handle int unsigned auto_increment PRIMARY KEY, 
dt_update TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
cd_product bigint(20), 
nr_available int(11), 
nr_session int(11), 
nr_order int(11), 
nr_price decimal(5,2));
