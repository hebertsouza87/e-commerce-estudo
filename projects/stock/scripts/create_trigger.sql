DELIMITER //
CREATE TRIGGER `stock`.`update_product` AFTER
UPDATE ON `stock`.`product`
FOR EACH ROW BEGIN IF ((OLD.nr_available <> new.nr_available)
OR (OLD.nr_session <> new.nr_session)
OR (OLD.nr_price <> new.nr_price)) THEN
INSERT INTO stock.product_move_to (`cd_product`, `nr_available`, `nr_session`, `nr_order`, `nr_price`)
VALUES (new.cd_product,
new.nr_available,
new.nr_session,
new.nr_order,
new.nr_price); END IF; END;
// DELIMITER ;