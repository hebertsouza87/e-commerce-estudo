package br.com.checkout.controller;

import br.com.checkout.config.ConfigEnum;
import br.com.checkout.domain.Order;
import br.com.checkout.service.CartService;
import br.com.checkout.service.PaymentService;
import br.com.checkout.service.PurchaseOrderService;
import br.com.commons.appConfig.AppConfig;
import br.com.commons.exception.ForbidenException;
import br.com.commons.json.CustomerJson;
import br.com.commons.json.OrderJson;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping(value = "/order")
public class OrderController {

    private static final Log LOG = LogFactory.getLog(OrderController.class);

    @Autowired
    private PurchaseOrderService purchaseOrderService;

    @Autowired
    private CartService cartService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AppConfig appConfig;

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{customerToken}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<List<OrderJson>> getAllOrders(@PathVariable("customerToken") String customerToken, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<OrderJson> orders = new ArrayList<>();
        CustomerJson customerJson = new CustomerJson();
        try {
            ResponseEntity<CustomerJson> entity = restTemplate.getForEntity(appConfig.getValue(ConfigEnum.ACCOUNT_ENDPOINT) + "/customer/" + customerToken, CustomerJson.class);
            customerJson = entity.getBody();
            if (!HttpStatus.OK.equals(entity.getStatusCode())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }
        List<Order> purchaseOrders = purchaseOrderService.findAllByCustomerId(customerJson.getId());
        if (purchaseOrders != null) {
            for (Order order : purchaseOrders) {
                orders.add(order.toJson());
            }
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{customerToken}/{order}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<OrderJson> getOrder(@PathVariable("customerToken") String customerToken, @PathVariable("order") String order, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Order purchaseOrder = purchaseOrderService.findById(Long.valueOf(order));
        try {
            ResponseEntity<CustomerJson> entity = restTemplate.getForEntity(appConfig.getValue(ConfigEnum.ACCOUNT_ENDPOINT) + "/customer/" + customerToken, CustomerJson.class);
            CustomerJson customerJson = entity.getBody();
            if (!HttpStatus.OK.equals(entity.getStatusCode())
                    || !purchaseOrder.getCustomerId().equals(customerJson.getId())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }
        return new ResponseEntity<>(purchaseOrder.toJson(), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "{customerToken}/{order}/viewTiket", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<byte[]> viewTiket(@PathVariable("customerToken") String customerToken, @PathVariable("order") String order, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Order purchaseOrder = purchaseOrderService.findById(Long.valueOf(order));
        try {
            ResponseEntity<CustomerJson> entity = restTemplate.getForEntity(appConfig.getValue(ConfigEnum.ACCOUNT_ENDPOINT) + "/customer/" + customerToken, CustomerJson.class);
            CustomerJson customerJson = entity.getBody();
            if (!HttpStatus.OK.equals(entity.getStatusCode())
                    || !purchaseOrder.getCustomerId().equals(customerJson.getId())) {
                throw new ForbidenException();
            }
        } catch (Exception e) {
            throw new ForbidenException();
        }

        byte[] ticket = paymentService.getTicket(purchaseOrder);

        response.setContentType("application/pdf");
        response.setContentLength(ticket.length);
        try (ServletOutputStream ouputStream = response.getOutputStream()) {
            ouputStream.write(ticket, 0, ticket.length);
            ouputStream.flush();
            ouputStream.close();
        }

        return new ResponseEntity<>(ticket, HttpStatus.OK);
    }

}
