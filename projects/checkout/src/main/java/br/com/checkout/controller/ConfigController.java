package br.com.checkout.controller;

import br.com.checkout.config.ConfigEnum;
import br.com.commons.appConfig.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ConfigController {

    @Autowired
    private AppConfig appConfig;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<String> updateProperties() throws Exception {
        appConfig.loadAppConfigProperties(ConfigEnum.values());
        return new ResponseEntity<>(AppConfig.getProperties().toString(), HttpStatus.OK);
    }

}
