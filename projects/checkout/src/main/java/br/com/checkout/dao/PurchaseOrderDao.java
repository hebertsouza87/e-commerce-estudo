package br.com.checkout.dao;

import br.com.checkout.domain.Order;
import br.com.commons.dao.AbstractDao;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED)
public class PurchaseOrderDao extends AbstractDao<Order> {

    @Override
    public Order save(Order purchaseOrder) {
        purchaseOrder.setId(null);
        return super.save(purchaseOrder);
    }

    public Order findById(Long id) {
        return super.findById(Order.class, id);
    }

    public Order update(Order purchaseOrder) {
        return super.update(purchaseOrder);
    }

    public void delete(Order purchaseOrder) {
        super.delete(purchaseOrder);
    }

    public List<Order> findAllByCustomerId(Long id) {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("customerId", id));

        List<Order> orders = super.findByRestrictions(Order.class, restrictions);

        return orders;
    }
}
