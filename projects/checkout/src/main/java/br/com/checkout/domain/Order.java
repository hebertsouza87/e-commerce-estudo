package br.com.checkout.domain;

import br.com.checkout.model.PaymentTypeEnum;
import br.com.commons.json.AddressJson;
import br.com.commons.json.Json;
import br.com.commons.json.OrderJson;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Index;
import org.joda.time.DateTime;

@Entity
@Table(name = "PURCHASE_ORDER")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Order implements Serializable {

    private static final long serialVersionUID = 1109826923361899489L;

    @Id
    @SequenceGenerator(name = "order_id", sequenceName = "order_id_seg")
    @GeneratedValue(generator = "order_id", strategy = GenerationType.AUTO)
    @Column(name = "CD_ORDER")
    private Long id;

    @Column(name = "CD_CUSTOMER", nullable = false)
    @Index(name = "ORDER_CD_CUSTOMER")
    private Long customerId;

    @Column(name = "CD_SELLER", nullable = false)
    @Index(name = "ORDER_CD_SELLER")
    private Long sellerId;

    @Column(name = "JSON_DELIVERY_ADDRESS", nullable = false, columnDefinition = "TEXT")
    private String deliveryAddress = "";

    @Column(name = "TRANSACTION", nullable = false, columnDefinition = "varchar(255)")
    private String transaction = "";

    @Column(name = "PAYMENT_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentTypeEnum paymentType;

    @Column(name = "DT_ORDER", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtOrder = DateTime.now().toDate();

    @Column(name = "BO_PAID", nullable = false)
    private boolean paid = false;

    @Column(name = "DT_PAID")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtPaid;

    @Column(name = "BO_PACKAGED", nullable = false)
    private boolean packaged = false;

    @Column(name = "DT_PACKAGED")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtPackaged;

    @Column(name = "BO_SENT", nullable = false)
    private boolean sent = false;

    @Column(name = "DT_SENT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtSent;

    @Column(name = "BO_DELIVERED", nullable = false)
    private boolean delivered = false;

    @Column(name = "DT_DELIVERED")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtDelivered;

    @OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "CD_CART", nullable = false)
    private Cart cart;

    public Order() {
    }

    public OrderJson toJson() throws Exception {
        OrderJson orderJson = new OrderJson();
        orderJson.setCart(getCart().toJson());
        orderJson.setCustomerId(getCustomerId());
        orderJson.setDeliveryAddress(getDeliveryAddress());
        orderJson.setId(getId());
        orderJson.setPaymentType(getPaymentType().getName());
        orderJson.setSellerId(getSellerId());
        orderJson.setTransaction(getTransaction());
        orderJson.setPaid(isPaid());
        orderJson.setPackaged(isPackaged());
        orderJson.setDelivered(isDelivered());
        orderJson.setDtDelivered(getDtDelivered());
        orderJson.setDtOrder(getDtOrder());
        orderJson.setDtPackaged(getDtPackaged());
        orderJson.setDtPaid(getDtPaid());
        orderJson.setSent(isSent());
        orderJson.setDtSent(getDtSent());

        return orderJson;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public AddressJson getObjectDeliveryAddress() {
        return Json.toObject(this.deliveryAddress, AddressJson.class);
    }

    public String getDeliveryAddress() {
        return this.deliveryAddress;
    }

    public void setDeliveryAddressJson(AddressJson addressJson) {
        this.deliveryAddress = Json.toJson(addressJson);
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public PaymentTypeEnum getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentTypeEnum paymentType) {
        this.paymentType = paymentType;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public boolean isPackaged() {
        return packaged;
    }

    public void setPackaged(boolean packaged) {
        this.packaged = packaged;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public Date getDtOrder() {
        return dtOrder;
    }

    public void setDtOrder(Date dtOrder) {
        this.dtOrder = dtOrder;
    }

    public Date getDtPaid() {
        return dtPaid;
    }

    public void setDtPaid(Date dtPaid) {
        this.dtPaid = dtPaid;
    }

    public Date getDtPackaged() {
        return dtPackaged;
    }

    public void setDtPackaged(Date dtPackaged) {
        this.dtPackaged = dtPackaged;
    }

    public Date getDtSent() {
        return dtSent;
    }

    public void setDtSent(Date dtSent) {
        this.dtSent = dtSent;
    }

    public Date getDtDelivered() {
        return dtDelivered;
    }

    public void setDtDelivered(Date dtDelivered) {
        this.dtDelivered = dtDelivered;
    }
}
