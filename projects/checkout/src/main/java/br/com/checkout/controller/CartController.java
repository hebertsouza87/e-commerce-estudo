package br.com.checkout.controller;

import br.com.checkout.config.ConfigEnum;
import br.com.checkout.domain.Cart;
import br.com.checkout.domain.Order;
import br.com.checkout.model.PaymentTypeEnum;
import br.com.checkout.service.CartService;
import br.com.checkout.service.FreightService;
import br.com.checkout.service.PaymentService;
import br.com.checkout.service.PurchaseOrderService;
import br.com.checkout.validator.CartValidator;
import br.com.commons.appConfig.AppConfig;
import br.com.commons.exception.ConflictException;
import br.com.commons.exception.InternalServerException;
import br.com.commons.json.AuthenticationTokenJson;
import br.com.commons.json.CartJson;
import br.com.commons.json.CustomerJson;
import br.com.commons.json.FreighJson;
import br.com.commons.json.PaymentJson;
import br.com.commons.json.ProductCartJson;
import br.com.commons.json.ProductCartListJson;
import br.com.commons.json.ProductJson;
import br.com.commons.json.ProductMovJson;
import br.com.commons.security.Encrypter;
import java.math.BigDecimal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping(value = "/cart")
public class CartController {

    private static final Log LOG = LogFactory.getLog(CartController.class);

    @Autowired
    private CartService cartService;

    @Autowired
    private FreightService freightService;

    @Autowired
    private PurchaseOrderService purchaseOrderService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private CartValidator cartValidator;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AppConfig appConfig;

    @RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<CartJson> getCart(@PathVariable("cartId") String cartId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));
        return new ResponseEntity<>(cart.toJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{cartId}/fullCart", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<CartJson> getFullCart(@PathVariable("cartId") String cartId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));
        ProductCartListJson cartListJson = new ProductCartListJson();
        for (ProductCartJson p : cart.getObjectProducts()) {
            ResponseEntity<ProductJson> getForEntity = restTemplate.getForEntity(appConfig.getValue(ConfigEnum.STOCK_ENDPOINT) + "product/" + p.getId(), ProductJson.class);
            ProductJson productJson = getForEntity.getBody();
            p.setName(productJson.getName());
            p.setPrice(productJson.getPrice());
            p.setWeight(productJson.getWeight());
            cartListJson.add(p);
        }
        cart.setProductsFromObjetc(cartListJson);
        cartService.update(cart);
        return new ResponseEntity<>(cart.toJson(), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<CartJson> create(@RequestBody CartJson cartJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        cartValidator.validate(cartJson);
        Cart cart = new Cart(cartJson);
        cart = cartService.create(cart);

        return new ResponseEntity<>(cart.toJson(), HttpStatus.CREATED);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{cartId}", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<ProductMovJson> addToCart(@PathVariable("cartId") String cartId, @RequestBody ProductMovJson productMovJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //verifica se o carrinho existe e as variáveis expired e finished
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));
        ProductCartJson product = cart.getProduct(productMovJson.getId());

        if (product != null) {
            throw new ConflictException();
        }

        productMovJson.setQuantity(1);

        //Chama o serviço do stock para reservar o produto
        ResponseEntity<ProductMovJson> postForEntity = restTemplate.postForEntity(appConfig.getValue(ConfigEnum.STOCK_ENDPOINT) + "product/stockToSession", productMovJson, ProductMovJson.class);
        productMovJson = postForEntity.getBody();

        //Adiciona o produto o produto (item ou quantidade)
        cart.addProduct(productMovJson);

        //Atualiza o carrinho com os dados completos
        cartService.update(cart);

        return new ResponseEntity<>(productMovJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{cartId}", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<ProductMovJson> updadeQuantity(@PathVariable("cartId") String cartId, @RequestBody ProductMovJson productMovJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //verifica se o carrinho existe e as variáveis expired e finished
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));
        ProductCartJson product = cart.getProduct(productMovJson.getId());

        if (productMovJson.getQuantity() > 20) {
            productMovJson.setQuantity(20);
        }

        productMovJson.setQuantity(productMovJson.getQuantity() - product.getQuantity());

        //Chama o serviço do stock para reservar o produto
        ResponseEntity<ProductMovJson> postForEntity = restTemplate.postForEntity(appConfig.getValue(ConfigEnum.STOCK_ENDPOINT) + "product/stockToSession", productMovJson, ProductMovJson.class);
        productMovJson = postForEntity.getBody();

        //Adiciona o produto o produto (item ou quantidade)
        cart.addProduct(productMovJson);

        //Atualiza o carrinho com os dados completos
        cartService.update(cart);

        return new ResponseEntity<>(productMovJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{cartId}/customer/", method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<CartJson> updadeCustomer(@PathVariable("cartId") String cartId, @RequestBody AuthenticationTokenJson authenticationTokenJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //verifica se o carrinho existe e as variáveis expired e finished
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));

        ResponseEntity<CustomerJson> entity = restTemplate.getForEntity(appConfig.getValue(ConfigEnum.ACCOUNT_ENDPOINT) + "/customer/" + authenticationTokenJson.getAuthenticationToken(), CustomerJson.class);
        CustomerJson customerJson = entity.getBody();

        cart.setCustomerId(customerJson.getId());
        cart = cartService.update(cart);

        return new ResponseEntity<>(cart.toJson(), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{cartId}/tiketPayment", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<PaymentJson> tiketPayment(@PathVariable("cartId") String cartId, @RequestBody PaymentJson paymantJson, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //verifica se o carrinho existe e as variáveis expired e finished
        Cart cart = cartService.verify(Long.valueOf(Encrypter.decrypt(cartId)));

        ResponseEntity<CustomerJson> entity = restTemplate.getForEntity(appConfig.getValue(ConfigEnum.ACCOUNT_ENDPOINT) + "/customer/" + paymantJson.getAuthenticationToken(), CustomerJson.class);
        CustomerJson customerJson = entity.getBody();

        cart.setCustomerId(customerJson.getId());
        FreighJson freighJson = freightService.freightCost(paymantJson.getAddress().getZipCode(), cart.getObjectProducts());
        cart.setFreightCost(BigDecimal.valueOf(freighJson.getPac()));
        cart = cartService.update(cart);

        Order order = new Order();
        order.setCart(cart);
        order.setCustomerId(cart.getCustomerId());
        order.setPaymentType(PaymentTypeEnum.TICKET);
        order.setSellerId(cart.getSellerId());
        order.setDeliveryAddressJson(paymantJson.getAddress());
        order = purchaseOrderService.create(order);

        try {
            String ticketPath = paymentService.generateTicket(order, customerJson);
            order.setTransaction(ticketPath);
            paymantJson.setAuthenticationToken(ticketPath);
            purchaseOrderService.update(order);

            cart.setFinished(true);
            cart = cartService.update(cart);

            for (ProductCartJson productCartJson : cart.getObjectProducts()) {
                //Movimenta o produto no estoque
                restTemplate.postForEntity(appConfig.getValue(ConfigEnum.STOCK_ENDPOINT) + "product/sessionToOrder", new ProductMovJson(productCartJson.getId(), productCartJson.getQuantity()), ProductMovJson.class);
            }

        } catch (Exception e) {
            if (order.getId() != null) {
                purchaseOrderService.delete(order);
                cart.setFinished(false);
                cartService.update(cart);
            }
            throw new InternalServerException();
        }

        paymantJson = new PaymentJson();
        paymantJson.setOrderToken(String.valueOf(order.getId()));

        return new ResponseEntity<>(paymantJson, HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{order}/viewTiket", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<byte[]> viewTiket(@PathVariable("tiket") String order, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Order purchaseOrder = purchaseOrderService.findById(Long.valueOf(order));
        byte[] ticket = paymentService.getTicket(purchaseOrder);

        return new ResponseEntity<>(ticket, HttpStatus.OK);
    }

}
