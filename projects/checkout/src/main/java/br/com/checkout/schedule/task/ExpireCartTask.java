package br.com.checkout.schedule.task;

import br.com.checkout.service.CartService;
import java.util.TimerTask;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ExpireCartTask extends TimerTask {

    private static final Log LOG = LogFactory.getLog(ExpireCartTask.class);

    private final CartService cartService;

    public ExpireCartTask(CartService CartService) {
        this.cartService = CartService;
    }

    @Override
    public void run() {
        try {
            cartService.expireCarts();
        } catch (Exception e) {
            LOG.error("Ocorreu um erro ao expirar os carrinhos", e);
        }
    }
}
