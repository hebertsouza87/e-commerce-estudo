package br.com.checkout.service;

import br.com.checkout.dao.PurchaseOrderDao;
import br.com.checkout.domain.Order;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PurchaseOrderService extends AbstractService {

    static final Logger LOG = Logger.getLogger(PurchaseOrderService.class);

    @Autowired
    private PurchaseOrderDao purchaseOrderDao;

    public Order create(Order purchaseOrder) throws Exception {
        purchaseOrder.setId(null);
        return purchaseOrderDao.save(purchaseOrder);
    }

    public Order update(Order purchaseOrder) {
        return purchaseOrderDao.update(purchaseOrder);
    }

    public void delete(Order order) {
        purchaseOrderDao.delete(order);
    }

    public Order findById(Long id) {
        return purchaseOrderDao.findById(id);
    }

    public List<Order> findAllByCustomerId(Long id) {
        return purchaseOrderDao.findAllByCustomerId(id);
    }

}
