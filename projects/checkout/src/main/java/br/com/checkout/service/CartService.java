package br.com.checkout.service;

import br.com.checkout.config.ConfigEnum;
import br.com.checkout.dao.CartDao;
import br.com.checkout.domain.Cart;
import br.com.commons.exception.ExpiredException;
import br.com.commons.exception.NotFoundException;
import br.com.commons.json.ProductCartJson;
import br.com.commons.json.ProductMovJson;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

public class CartService extends AbstractService {

    static final Logger LOG = Logger.getLogger(CartService.class);

    @Autowired
    private CartDao cartDao;

    public Cart create(Cart cart) throws Exception {
        cart.setId(null);
        cart.setDtStart(DateTime.now().toDate());
        cart.setDtToExpire(getDateToExpire());
        return cartDao.save(cart);
    }

    private Date getDateToExpire() {
        return DateTime.now().plusMinutes(appConfig.getValueAsInteger(ConfigEnum.TIME_TO_EXPIRE_CART)).toDate();
    }

    public void expireCarts() throws Exception {
        List<Cart> carts = cartDao.findAllCartsToExpire();
        if (carts != null && !carts.isEmpty()) {
            for (Cart cart : carts) {
                expireCart(cart);
            }
        }
    }

    private void expireCart(Cart cart) throws Exception {
        if (cart.getObjectProducts() != null) {
            ArrayList<ProductMovJson> success = new ArrayList<>();
            ArrayList<ProductMovJson> fail = new ArrayList<>();
            try {
                for (ProductCartJson productCartJson : cart.getObjectProducts()) {
                    //Chama o serviço do stock para devolver os itens ao stock
                    ProductMovJson productMovJson = new ProductMovJson(productCartJson.getId(), productCartJson.getQuantity());
                    try {
                        //transforma o valor em negativo para que seja feito a devolução
                        productMovJson.setQuantity(productMovJson.getQuantity() * -1);
                        restTemplate.postForEntity(appConfig.getValue(ConfigEnum.STOCK_ENDPOINT) + "product/stockToSession", productMovJson, ProductMovJson.class);
                        success.add(productMovJson);
                    } catch (Exception e) {
                        fail.add(productMovJson);
                    }
                }
                if (fail.isEmpty()) {
                    cartDao.expire(cart);
                } else {
                    for (ProductMovJson productMovJson : success) {
                        try {
                            //transforma o valor em posito para que seja feito a baixa do estoque novamente
                            productMovJson.setQuantity(Math.abs(productMovJson.getQuantity()));
                            restTemplate.postForEntity(appConfig.getValue(ConfigEnum.STOCK_ENDPOINT) + "product/stockToSession", productMovJson, ProductMovJson.class);
                            success.add(productMovJson);
                        } catch (Exception e) {
                            fail.add(productMovJson);
                        }
                    }
                }
            } catch (Exception e) {
                for (ProductMovJson productMovJson : success) {
                    try {
                        //transforma o valor em posito para que seja feito a baixa do estoque novamente
                        productMovJson.setQuantity(Math.abs(productMovJson.getQuantity()));
                        restTemplate.postForEntity(appConfig.getValue(ConfigEnum.STOCK_ENDPOINT) + "product/stockToSession", productMovJson, ProductMovJson.class);
                        success.add(productMovJson);
                    } catch (Exception ex) {
                        fail.add(productMovJson);
                    }
                }
            }
        }
    }

    public Cart verify(Long id) throws Exception {
        Cart cart = cartDao.findById(id);
        if (cart == null) {
            throw new NotFoundException("Carrinho não encontrado");
        }
        if (cart.isExpired() || cart.isFinished()) {
            throw new ExpiredException("Carrinho " + (cart.isExpired() ? "expirado" : "finalizado"));
        } else {
            cart = renewCart(cart);
        }
        return cart;
    }

    private Cart renewCart(Cart cart) {
        cart.setDtToExpire(getDateToExpire());
        return cartDao.update(cart);
    }

    public Cart update(Cart cart) {
        return cartDao.update(cart);
    }

    public Cart findById(Long id) {
        return cartDao.findById(id);
    }

}
