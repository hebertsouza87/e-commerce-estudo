package br.com.checkout.config;

import br.com.commons.appConfig.ConfigEnumInterface;

public enum ConfigEnum implements ConfigEnumInterface<ConfigEnum> {

    ENDPOINT(1L, "ENDPOINT", "http://localhost:8080/checkout/"),
    STOCK_ENDPOINT(2L, "STOCK_ENDPOINT", "http://localhost:8080/stock/"),
    TIME_TO_EXPIRE_CART(3L, "TIME_TO_EXPIRE_CART", "30"),
    TIME_TO_EXECUTE_EXPIRE_CART(4L, "TIME_TO_EXECUTE_EXPIRE_CART", "15"),
    TICKET_PATH(5L, "TICKET_PATH", "C:\\boletos\\"),
    ACCOUNT_ENDPOINT(6L, "ACCOUNT_ENDPOINT", "http://localhost:8080/account/"),
    SHOP_ZIP_CODE(7L, "SHOP_ZIP_CODE", "21351021");

    private final Long id;
    private final String name;
    private final String defaultValue;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    private ConfigEnum(Long id, String key, String defaultValue) {
        this.id = id;
        this.name = key;
        this.defaultValue = defaultValue;
    }

    @Override
    public ConfigEnum[] getValues() {
        return values();
    }
}
