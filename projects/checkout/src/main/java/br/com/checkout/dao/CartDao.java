package br.com.checkout.dao;

import br.com.checkout.domain.Cart;
import br.com.commons.dao.AbstractDao;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED)
public class CartDao extends AbstractDao<Cart> {

    @Override
    public Cart save(Cart cart) {
        cart.setId(null);
        return super.save(cart);
    }

    public List<Cart> findAllCartsToExpire() {
        List<Criterion> restrictions = new ArrayList<>();
        restrictions.add(Restrictions.eq("finished", false));
        restrictions.add(Restrictions.eq("expired", false));
        restrictions.add(Restrictions.le("dtToExpire", DateTime.now().toDate()));

        List<Cart> carts = super.findByRestrictions(Cart.class, restrictions);

        return carts;
    }

    public void expire(List<Cart> carts) {
        if (carts != null) {
            for (Cart cart : carts) {
                expire(cart);
            }
        }
    }

    public void expire(Cart cart) {
        cart.setDtExpired(DateTime.now().toDate());
        cart.setExpired(true);
        update(cart);
    }

    public Cart findById(Long id) {
        return super.findById(Cart.class, id);
    }

    public Cart update(Cart cart) {
        return super.update(cart);
    }
}
