package br.com.checkout.schedule;

import br.com.checkout.config.ConfigEnum;
import br.com.checkout.schedule.task.ExpireCartTask;
import br.com.checkout.service.CartService;
import br.com.commons.appConfig.AppConfig;
import br.com.commons.schedule.AbstractScheduleManager;
import javax.annotation.PostConstruct;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class CheckoutScheduleManager extends AbstractScheduleManager {

    private static final Log LOG = LogFactory.getLog(CheckoutScheduleManager.class);
    private ExpireCartTask expireCartTask;

    private final CartService cartService;
    private final AppConfig appConfig;

    public CheckoutScheduleManager(CartService cartService, AppConfig appConfig) {
        this.cartService = cartService;
        this.appConfig = appConfig;

        schedule();
    }

    @Override
    @PostConstruct
    public void schedule() {
        scheduleExpireCart();
    }

    private void scheduleExpireCart() {
        cancelSchedule(expireCartTask);
        expireCartTask = new ExpireCartTask(cartService);
//        scheduleTask(expireCartTask, 1L, 30L);
        scheduleTask(expireCartTask, appConfig.getValueLongDefault(ConfigEnum.TIME_TO_EXECUTE_EXPIRE_CART, 1L), appConfig.getValueLongDefault(ConfigEnum.TIME_TO_EXPIRE_CART, 30L));
    }
}
