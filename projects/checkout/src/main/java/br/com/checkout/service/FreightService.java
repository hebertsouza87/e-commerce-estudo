package br.com.checkout.service;

import br.com.checkout.config.ConfigEnum;
import br.com.commons.json.FreighJson;
import br.com.commons.json.ProductCartJson;
import java.math.BigDecimal;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;

public class FreightService extends AbstractService {

    static final Logger LOG = Logger.getLogger(FreightService.class);

    public FreighJson freightCost(String zipCode, Double value, Long weight) {
        FreighJson freighJson = new FreighJson();
        if (weight.equals(0L)) {
            return freighJson;
        }
        String kg = String.valueOf(Double.valueOf(weight) / 1000).replace('.', ',');

        ResponseEntity<String> getForEntity = restTemplate.getForEntity("https://pagseguro.uol.com.br/desenvolvedor/simulador_de_frete_calcular.jhtml?postalCodeFrom="
                + appConfig.getValue(ConfigEnum.SHOP_ZIP_CODE) + "&weight=" + kg + "&value=" + value + "&postalCodeTo=" + zipCode, String.class
        );
        String response = getForEntity.getBody();
        String[] splitedResponse = response.split("\\|");
        if (splitedResponse != null
                && splitedResponse.length >= 5) {
            freighJson.setPac(Double.valueOf(splitedResponse[4].trim()));
            return freighJson;
        }
        return freighJson;
    }

    public FreighJson freightCost(String zipCode, List<ProductCartJson> products) {
        BigDecimal totalPrice = BigDecimal.ZERO;
        Long totalWeight = 0L;
        FreighJson freighJson = new FreighJson();
        if (products != null) {

            for (ProductCartJson product : products) {
//                totalPrice.add(product.getPrice()  * product.getQuantity()); // No momento não será informado o valor do produto, pois está aumentando em muito o custo do frete
                totalWeight += product.getWeight() * product.getQuantity();
            }
            freighJson = freightCost(zipCode, Double.valueOf(totalPrice.toString()), totalWeight);
        }

        return freighJson;
    }

}
