package br.com.commons.security;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

public class AuthenticationTest {

    @Test
    public void getAuthenticationCode() throws Exception {
        DateTime dateTime = new DateTime(1450736288265l);
        Authentication authentication = new Authentication("10", AuthenticationRole.SELLER, dateTime);
        String authenticationCode = authentication.getAuthenticationCode();

        Authentication recoveredAuthentication = Authentication.fromString(authenticationCode);
        Assert.assertEquals("10", recoveredAuthentication.getId());
        Assert.assertEquals(AuthenticationRole.SELLER, recoveredAuthentication.getAuthenticationRole());
        Assert.assertEquals(dateTime, recoveredAuthentication.getExpirationDate());
    }

}
