package br.com.commons.domain;

import br.com.commons.appConfig.ConfigEnumInterface;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "APP_CONFIG")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AppConfigDomain implements Serializable {

    private static final long serialVersionUID = 8746387L;

    @Id
    @Column(name = "CD_APP_CONFIG")
    private Long id;

    @Column(name = "NAME", nullable = false, unique = true, length = 255)
    private String name;

    @Column(name = "VALUE", nullable = false, length = 255)
    private String value;

    public AppConfigDomain() {
    }

    public AppConfigDomain(ConfigEnumInterface configEnum) {
        this.id = configEnum.getId();
        this.name = configEnum.getName();
        this.value = configEnum.getDefaultValue();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
