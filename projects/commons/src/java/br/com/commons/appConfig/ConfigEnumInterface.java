package br.com.commons.appConfig;

public interface ConfigEnumInterface<T> {

    public Long getId();

    public String getName();

    public String getDefaultValue();

    public T[] getValues();

}
