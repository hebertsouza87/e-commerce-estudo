package br.com.commons.appConfig;

import br.com.commons.dao.AppConfigDao;
import br.com.commons.domain.AppConfigDomain;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AppConfig {

    private final static Log LOG = LogFactory.getLog(AppConfig.class);

    private static final Map<Long, String> properties = new HashMap<>();

    @Autowired
    private AppConfigDao appConfigDao;

    @PostConstruct
    public String getValue(ConfigEnumInterface configEnum) {
        try {
            if (!properties.containsKey(configEnum.getId())) {
                findAndSet(configEnum);
            }
        } catch (Exception e) {
            LOG.error("Error ao carregar o valor do appConfig para o item: " + configEnum.getName());
        }

        return properties.get(configEnum.getId());
    }

    @PostConstruct
    public Long getValueLongDefault(ConfigEnumInterface configEnum, Long def) {
        try {
            if (!properties.containsKey(configEnum.getId())) {
                findAndSet(configEnum);
            }
        } catch (Exception e) {
            LOG.error("Error ao carregar o valor do appConfig para o item: " + configEnum.getName());
        }

        String get = properties.get(configEnum.getId());

        if (StringUtils.isAnyBlank(get)) {
            return def;
        }
        return Long.valueOf(get);
    }

    public Long getValueAsLong(ConfigEnumInterface configEnum) {
        return Long.valueOf(getValue(configEnum));
    }

    public int getValueAsInteger(ConfigEnumInterface configEnum) {
        return Integer.valueOf(getValue(configEnum));
    }

    public void loadAppConfigProperties(ConfigEnumInterface[] configEnumValues) {
        try {
            properties.clear();
            for (ConfigEnumInterface configEnumValue : configEnumValues) {
                findAndSet(configEnumValue);
            }

        } catch (Exception e) {
            throw new RuntimeException("Appconfig nao pode ser carregado");
        }
    }

    public static Map<Long, String> getProperties() {
        return properties;
    }

    private void findAndSet(ConfigEnumInterface configEnum) {
        AppConfigDomain config = appConfigDao.findById(configEnum.getId());
        if (config == null) {
            config = new AppConfigDomain(configEnum);
            config = appConfigDao.save(config);
        }

        properties.put(config.getId(), config.getValue());
    }
}
