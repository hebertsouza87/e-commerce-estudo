package br.com.commons.dao;

import br.com.commons.domain.AppConfigDomain;
import java.util.List;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED)
public class AppConfigDao extends AbstractDao<AppConfigDomain> {

    @Override
    public AppConfigDomain save(AppConfigDomain appConfigDomain) {
        return super.save(appConfigDomain);
    }

    public AppConfigDomain findById(Long id) {
        return super.findById(AppConfigDomain.class, id);
    }

    public List<AppConfigDomain> findAll() {
        return super.findAll(AppConfigDomain.class);
    }

}
