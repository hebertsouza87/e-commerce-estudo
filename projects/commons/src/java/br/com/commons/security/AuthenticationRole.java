package br.com.commons.security;

public enum AuthenticationRole {

    MANAGER,
    SELLER,
    CUSTOMER,
    NONE;

}
