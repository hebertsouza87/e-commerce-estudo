package br.com.commons.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Encrypter {

    private static final Log LOG = LogFactory.getLog(Encrypter.class);

    private static final String cryptKey = "CB21BFF0096D08CA20FBC1EF0B78AC03";
    private static final String vector = "3DEC7C9ECC1ADA1EE9AA7AFB2F9CDE80";

    private static Cipher decrypter;
    private static Cipher crypter;

    private static final Base64 base64 = new Base64(true);
    private static MessageDigest md;

    public static String encrypt(String string) throws Exception {
        try {
            if (string == null) {
                return null;
            }

            byte[] encryptedByteArray = getCrypter().doFinal(string.getBytes());
            return new String(base64.encode(encryptedByteArray)).trim();
        } catch (Exception e) {
            LOG.error("Nao foi possivel criptografar o texto: [" + string + "]", e);
            return null;
        }
    }

    public static String decrypt(String hash) throws Exception {
        try {
            if (hash == null) {
                return null;
            }

            byte[] decodedHashBytes = base64.decode(hash.getBytes());
            byte[] decryptedByteArray = getDecrypter().doFinal(decodedHashBytes);
            return new String(decryptedByteArray);
        } catch (Exception e) {
            LOG.info("Nao foi possivel descriptografar o hash [" + hash + "]", e);
            return null;
        }
    }

    private static Cipher getCrypter() throws Exception {
        if (crypter == null) {
            crypter = getCipher(Cipher.ENCRYPT_MODE);
        }

        return crypter;
    }

    private static Cipher getDecrypter() throws Exception {
        if (decrypter == null) {
            decrypter = getCipher(Cipher.DECRYPT_MODE);
        }

        return decrypter;
    }

    private static Cipher getCipher(int mode) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(mode, new SecretKeySpec(hexStringToByteArray(cryptKey), "AES"), new IvParameterSpec(hexStringToByteArray(vector)));
        return cipher;
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();

        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }

    /**
     * Este tipo de encryptação não tem retorno.
     *
     * @param password
     * @return hash encryptado do password.
     */
    public static String getEncryptedPassword(String password) {
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] passBytes = password.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < digested.length; i++) {
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            LOG.info("Nao foi possivel encriptar o password [" + password + "]", e);
        }
        return null;
    }
}
