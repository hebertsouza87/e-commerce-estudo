package br.com.commons.exception;

import org.springframework.http.HttpStatus;

public class BadRequestException extends RestHttpException {

    private static final long serialVersionUID = 4237276476234L;

    public BadRequestException(String message) {
        super("Request mal formatado: " + message, HttpStatus.BAD_REQUEST);
    }
}
