package br.com.commons.exception;

import br.com.commons.json.Response;
import br.com.commons.validator.ValidationError;
import java.util.List;
import javax.servlet.ServletException;
import org.springframework.http.HttpStatus;

public abstract class RestHttpException extends ServletException {

    private static final long serialVersionUID = 1235572736437684567L;

    private HttpStatus status;
    private List<ValidationError> errors;

    public RestHttpException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public RestHttpException(String message, HttpStatus status, List<ValidationError> errros) {
        super(message);
        this.status = status;
        this.errors = errros;
    }

    public Integer getStatus() {
        return status.value();
    }

    public List<ValidationError> getErrors() {
        return errors;
    }

    public String toJson() {
        return new Response(status.value(), getMessage(), errors).toJson();
    }

    public String to(String[] accepts) {
        for (String acceptHeader : accepts) {
            if (acceptHeader.equals("application/json")) {
                return toJson();
            }
        }
        return toJson();
    }
}
