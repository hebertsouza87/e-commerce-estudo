package br.com.commons.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends RestHttpException {

    private static final long serialVersionUID = 4275476546458387687L;

    public NotFoundException() {
        this("Não encontrado");
    }

    public NotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }

}
