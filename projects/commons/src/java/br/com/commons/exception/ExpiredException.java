package br.com.commons.exception;

import org.springframework.http.HttpStatus;

public class ExpiredException extends RestHttpException {

    private static final long serialVersionUID = 134536647464345L;

    public ExpiredException() {
        this("Recurso expirado");
    }

    public ExpiredException(String message) {
        super(message, HttpStatus.GONE);
    }
}
