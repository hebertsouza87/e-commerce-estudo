package br.com.commons.exception;

import br.com.commons.validator.ValidationError;
import java.util.List;
import org.springframework.http.HttpStatus;

public class UnprocessableEntityException extends RestHttpException {

    private static final long serialVersionUID = 816836451803283536L;

    public UnprocessableEntityException(String message, List<ValidationError> validationErrors) {
        super(message, HttpStatus.UNPROCESSABLE_ENTITY, validationErrors);
    }
}
