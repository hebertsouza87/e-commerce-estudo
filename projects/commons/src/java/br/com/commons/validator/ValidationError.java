package br.com.commons.validator;

public class ValidationError {

    private String field;
    private RestrictionType restrictionType;
    private String message;

    public ValidationError(String field, RestrictionType restrictionType, String message) {
        this.field = field;
        this.restrictionType = restrictionType;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public RestrictionType getRestrictionType() {
        return restrictionType;
    }

    public void setRestrictionType(RestrictionType restrictionType) {
        this.restrictionType = restrictionType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
