package br.com.commons.validator;

public enum RestrictionType {

    INVALID("INVALID"),
    REQUIRED("REQUIRED"),
    EXACT_LENGTH("EXACT_LENGTH");

    private RestrictionType(String type) {
        this.type = type;
    }

    private String type;
}
