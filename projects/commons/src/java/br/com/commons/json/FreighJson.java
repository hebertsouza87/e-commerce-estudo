package br.com.commons.json;

import java.io.Serializable;

public class FreighJson implements Serializable {

    private static final long serialVersionUID = 1193093501588924278L;

    private Double pac;
    private Long pacDays;
    private Double sedex;
    private Long sedexDays;

    public FreighJson() {
    }

    public Double getPac() {
        return pac;
    }

    public void setPac(Double pac) {
        this.pac = pac;
    }

    public Long getPacDays() {
        return pacDays;
    }

    public void setPacDays(Long pacDays) {
        this.pacDays = pacDays;
    }

    public Double getSedex() {
        return sedex;
    }

    public void setSedex(Double sedex) {
        this.sedex = sedex;
    }

    public Long getSedexDays() {
        return sedexDays;
    }

    public void setSedexDays(Long sedexDays) {
        this.sedexDays = sedexDays;
    }

}
