package br.com.commons.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.math.BigDecimal;

@JsonSerialize
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PerfumeJson extends ProductJson implements Serializable {

    private static final long serialVersionUID = 31243464782364872L;
        
    private String recomendedGender;
    private String inspiration;        
    private Long typeId;        
    private String intensity;        
    private BigDecimal size;

    public PerfumeJson() {
    }
    
    public PerfumeJson(ProductJson productJson){
        super(productJson);
    }

    public PerfumeJson(PerfumeJson perfumeJson) {
        if (perfumeJson != null){
            this.recomendedGender = perfumeJson.getRecomendedGender();
            this.inspiration = perfumeJson.getInspiration();
            this.typeId = perfumeJson.getTypeId();
            this.intensity = perfumeJson.getIntensity();
            this.size = perfumeJson.getSize();
        }
                
    }

    public String getRecomendedGender() {
        return recomendedGender;
    }

    public void setRecomendedGender(String recomendedGender) {
        this.recomendedGender = recomendedGender;
    }

    public String getInspiration() {
        return inspiration;
    }

    public void setInspiration(String inspiration) {
        this.inspiration = inspiration;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getIntensity() {
        return intensity;
    }

    public void setIntensity(String intensity) {
        this.intensity = intensity;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }           

}
