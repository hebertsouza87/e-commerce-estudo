package br.com.commons.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.math.BigDecimal;

@JsonSerialize
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductJson extends ProductCartJson implements Serializable {

    private static final long serialVersionUID = 34953248573L;

    private String name;
    private Long categoryId;
    private String imageUrl;
    private BigDecimal price;
    private String description;
    private String shotDescription;
    private String code;
    private Long weight = 0L;
    private Integer available = 0;
    private Integer session = 0;
    private Integer order = 0;
    private Integer sold = 0;
    private Integer stock = 0;

    public ProductJson() {
    }

    public ProductJson(ProductJson productJson) {        
        this.name = productJson.getName();
        this.categoryId = productJson.getCategoryId();
        this.imageUrl = productJson.getImageUrl();
        this.price = productJson.getPrice();
        this.description = productJson.getDescription();
        this.shotDescription = productJson.getShotDescription();
        this.code = productJson.getCode();
        this.weight = productJson.getWeight();
        this.available = productJson.getAvailable();
        this.session = productJson.getSession();
        this.order = productJson.getOrder();
        this.sold = productJson.getSold();
        this.stock = productJson.getStock();
    }
    
    public String getName() {
        return name;
    }
    
    public BigDecimal getPrice(){
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public String getShotDescription() {
        return shotDescription;
    }

    public String getCode() {
        return code;
    }

    public Long getWeight() {
        return weight;
    }

    public Integer getAvailable() {
        return available;
    }

    public Integer getSession() {
        return session;
    }

    public Integer getOrder() {
        return order;
    }

    public Integer getSold() {
        return sold;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setShotDescription(String shotDescription) {
        this.shotDescription = shotDescription;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public void setSession(Integer session) {
        this.session = session;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }    
    
}
