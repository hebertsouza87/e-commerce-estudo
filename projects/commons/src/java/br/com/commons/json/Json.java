package br.com.commons.json;

import com.google.gson.Gson;

public final class Json {

    private static final Gson GSON = new Gson();

    @SuppressWarnings("unchecked")
    public static final <T> T toObject(String json, Class<T> type) {
        Object object = GSON.fromJson(json, type);
        return (T) object;
    }

    public static String toJson(Object object) {
        return GSON.toJson(object);
    }

}
