package br.com.commons.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Date;

@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderJson implements Serializable {

    private static final long serialVersionUID = -120694022110489496L;

    private Long id;
    private Long customerId;
    private Long sellerId;
    private String deliveryAddress = "";
    private String transaction = "";
    private String paymentType;
    private Date dtOrder;
    private boolean paid;
    private Date dtPaid;
    private boolean packaged;
    private Date dtPackaged;
    private boolean sent;
    private Date dtSent;
    private boolean delivered;
    private Date dtDelivered;

    private CartJson cart;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public CartJson getCart() {
        return cart;
    }

    public void setCart(CartJson cart) {
        this.cart = cart;
    }

    public Date getDtOrder() {
        return dtOrder;
    }

    public void setDtOrder(Date dtOrder) {
        this.dtOrder = dtOrder;
    }

    public Date getDtPaid() {
        return dtPaid;
    }

    public void setDtPaid(Date dtPaid) {
        this.dtPaid = dtPaid;
    }

    public boolean isPackaged() {
        return packaged;
    }

    public void setPackaged(boolean packaged) {
        this.packaged = packaged;
    }

    public Date getDtPackaged() {
        return dtPackaged;
    }

    public void setDtPackaged(Date dtPackaged) {
        this.dtPackaged = dtPackaged;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public Date getDtSent() {
        return dtSent;
    }

    public void setDtSent(Date dtSent) {
        this.dtSent = dtSent;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public Date getDtDelivered() {
        return dtDelivered;
    }

    public void setDtDelivered(Date dtDelivered) {
        this.dtDelivered = dtDelivered;
    }

}
