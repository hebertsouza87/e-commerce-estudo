package br.com.commons.json;

import java.io.Serializable;

public class AccountJson implements Serializable {

    private static final long serialVersionUID = 6160091771122901875L;

    private Long id;
    private String name;
    private String nickName;
    private String email;
    private String password;
    private String cpf;
    private String authenticationToken = "";

    public AccountJson() {
    }

    public AccountJson(AccountJson accountJson) {
        if (accountJson != null) {
            this.id = accountJson.getId();
            this.name = accountJson.getName();
            this.nickName = accountJson.getNickName();
            this.email = accountJson.getEmail();
            this.password = accountJson.getPassword();
            this.cpf = accountJson.getCpf();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }
}
