package br.com.commons.json;

import br.com.commons.security.Encrypter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Date;

@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CartJson implements Serializable {

    private static final long serialVersionUID = -9204078056505107746L;

    private Long id;
    private Long customerId;
    private Long sellerId;
    private ProductCartListJson products;
    private Date dtStart;
    private Date dtExpired;
    private boolean finished;
    private boolean expired;
    private String encryptId;
    private AddressJson address;
    private Double freightCost = 0.0D;
    private Double totalQuantityProducts = 0.0D;
    private Double totalPriceProducts = 0.0D;

    public String getEncryptId() {
        return encryptId;
    }

    public void setEncryptId(Long id) throws Exception {
        this.encryptId = Encrypter.encrypt(String.valueOf(id));
    }

    public CartJson() {
    }

    public CartJson(Long cartId) {
        this.id = cartId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCostumerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public ProductCartListJson getProducts() {
        return products;
    }

    public void setProducts(ProductCartListJson products) {
        this.products = products;
    }

    public Date getDtStart() {
        return dtStart;
    }

    public void setDtStart(Date dtStart) {
        this.dtStart = dtStart;
    }

    public Date getDtExpired() {
        return dtExpired;
    }

    public void setDtExpired(Date dtExpired) {
        this.dtExpired = dtExpired;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public AddressJson getAddress() {
        return address;
    }

    public void setAddress(AddressJson address) {
        this.address = address;
    }

    public Double getFreightCost() {
        return freightCost;
    }

    public void setFreightCost(Double freightCost) {
        this.freightCost = freightCost;
    }

    public Double getTotalQuantityProducts() {
        this.totalQuantityProducts = 0D;
        if (products != null) {
            products.stream().forEach((product) -> {
                totalQuantityProducts += product.getQuantity();
            });
        }

        return totalQuantityProducts;
    }

    public void setTotalQuantityProducts(Double totalQuantityProducts) {
        this.totalQuantityProducts = totalQuantityProducts;
    }

    public Double getTotalPriceProducts() {
        this.totalPriceProducts = 0.0D;
        if (products != null) {
            products.stream().forEach((product) -> {
                totalPriceProducts += product.getPrice().doubleValue() * product.getQuantity();
            });
        }

        return totalPriceProducts;
    }

    public void setTotalPriceProducts(Double totalPriceProducts) {
        this.totalPriceProducts = totalPriceProducts;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }
}
