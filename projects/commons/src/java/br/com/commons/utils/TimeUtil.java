package br.com.commons.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class TimeUtil {

    public static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static DateTime now() {
        return new DateTime();
    }

    public static String toString(DateTime date) {
        if (date == null) {
            return null;
        }

        return date.toString(DEFAULT_FORMAT);
    }

    public static DateTime toDate(String dateAsString) {
        if (dateAsString == null) {
            return null;
        }

        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern(DEFAULT_FORMAT);
            return formatter.parseDateTime(dateAsString);
        } catch (Exception e) {
            return null;
        }
    }

    public static Long minutesToMilliseconds(Long minutes) {
        final int SECONDS_PER_MINUTE = 60;
        final int MILLISECONDS_PER_SECOND = 1000;

        return minutes * SECONDS_PER_MINUTE * MILLISECONDS_PER_SECOND;
    }

}
